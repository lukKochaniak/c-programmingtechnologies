﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer {
    public class BookCatalog : IBookCatalog {
        public List<Book> Books { get; set; }
        public List<Reader> Readers { get; set; }
        public List<Rental> Rentals { get; set; }

        public BookCatalog() {
            Books = new List<Book>();
            Readers = new List<Reader>();
            Rentals = new List<Rental>();
        }

        public BookCatalog(List<Book> Books) {
            this.Books = Books;
        }

        public BookCatalog(List<Book> Books, List<Rental> Rentals, List<Reader> Readers) {
            this.Books = Books;
            this.Rentals = Rentals;
            this.Readers = Readers;
        }
    }
}
