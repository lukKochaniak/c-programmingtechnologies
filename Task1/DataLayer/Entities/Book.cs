﻿using System;

namespace DataLayer {
    [Serializable]
    public class Book {
        public int Id { set; get; }
        public string Title { set; get; }
        public Writer Author { set; get; }
        public int Pages { set; get; }
        public int Year { set; get; }
        public int Quantity { set; get; }

        public Book(int Id, string Title, Writer Author, int Pages, int Year) {
            this.Id = Id;
            this.Title = Title;
            this.Author = Author;
            this.Pages = Pages;
            this.Year = Year;
            this.Quantity = 0;
        }

    }
}
