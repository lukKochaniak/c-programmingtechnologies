﻿using System;

namespace DataLayer {
    [Serializable]
    public class Reader {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string FlatNumber { get; set; }

        public Reader(string Name, string Surname, string City, string Street, string StreetNumber, string FlatNumber) {
            this.Name = Name;
            this.Surname = Surname;
            this.City = City;
            this.Street = Street;
            this.StreetNumber = StreetNumber;
            this.FlatNumber = FlatNumber;
        }
    }
}
