﻿using System;

namespace DataLayer {
    [Serializable]
    public class Writer {
        public string Name { set; get; }
        public string Surname { set; get; }

        public Writer(string Name, string Surname) {
            this.Name = Name;
            this.Surname = Surname;
        }
    }
}
