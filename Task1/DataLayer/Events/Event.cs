﻿namespace DataLayer.Events {
    public abstract class Event {
        public delegate void AfterEventExecution(Event ievenet);
        public event AfterEventExecution afterEventExecution;
        public string eventDescription;

        public void eventExecution(Event ievenet) {
            if (afterEventExecution != null)
                afterEventExecution(ievenet);
        }

        public string EventDescription() {
            return eventDescription;
        }
    }
}
