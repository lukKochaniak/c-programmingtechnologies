﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class AddBookEvent : Event {

        public bool Execute(IBookCatalog BookCatalog, Book Book) {
            List<Book> Books = BookCatalog.Books;
            //Book.Id = Books.Count;

            if (Books.Any(x => x.Id == Book.Id)) {
                Book book = Books.Find(x => x.Id == Book.Id);
                if (book.Title != Book.Title || book.Pages != Book.Pages || book.Year != Book.Year)
                    return false;
                Books.Find(x => x.Id == Book.Id).Quantity++;
                eventExecution(this);
                eventDescription = "Increased qunatity of a book with id: " + Book.Id + " and title: " + Book.Title;
                return true;
            } else {
                Book.Quantity++;
                Books.Add(Book);
                eventExecution(this);
                eventDescription = "Added book with id: " + Book.Id + " and title: " + Book.Title;
                return true;
            }
        }
    }
}
