﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class AddReaderEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Reader Reader) {
            List<Reader> Readers = BookCatalog.Readers;
            if (Readers.Contains(Reader)) {
                return false;
            } else {
                Readers.Add(Reader);
                eventExecution(this);
                eventDescription = "Added Reader " + Reader.Name + " " + Reader.Surname;
                return true;
            }
        }
    }
}
