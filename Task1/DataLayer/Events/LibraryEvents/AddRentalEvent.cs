﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class AddRentalEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Rental Rental) {
            List<Rental> Rentals = BookCatalog.Rentals;
            if (Rentals.Contains(Rental)) {
                return false;
            } else {
                Rentals.Add(Rental);
                eventExecution(this);
                eventDescription = "Added Rental on name: " + Rental.Reader.Name + " " + Rental.Reader.Surname + ", date: " + Rental.RentalDate;
                return true;
            }
        }
    }
}
