﻿using System.Collections.Generic;

namespace DataLayer.Events {
    class RemoveBookEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Book Book) {
            List<Book> Books = BookCatalog.Books;
            eventExecution(this);
            eventDescription = "Removed book: " + Book.Title;
            return Books.Remove(Book);
        }
    }
}
