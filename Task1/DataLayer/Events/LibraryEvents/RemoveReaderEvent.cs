﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class RemoveReaderEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Reader Reader) {
            List<Reader> Readers = BookCatalog.Readers;
            eventExecution(this);
            eventDescription = "Removed reader: " + Reader.Name + " " + Reader.Surname;
            return Readers.Remove(Reader);
        }
    }
}
