﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class RemoveRentalEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Rental Rental) {
            List<Rental> Rentals = BookCatalog.Rentals;
            eventExecution(this);
            eventDescription = "Removed Rental on name: " + Rental.Reader.Name + " " + Rental.Reader.Surname + ", date: " + Rental.RentalDate + ", expiration date: " + Rental.ExpireDate;
            return Rentals.Remove(Rental);
        }
    }
}
