﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class RenewRentalEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Rental Rental, DateTime RenewTo) {
            List<Rental> Rentals = BookCatalog.Rentals;
            int index = Rentals.IndexOf(Rental);
            if (index != -1) {
                Rentals[index].ExpireDate = RenewTo;
                eventExecution(this);
                eventDescription = "Renewd Rental on name: " + Rental.Reader.Name + " " + Rental.Reader.Surname + ", date: " + Rental.RentalDate + ", new expiration date: " + Rental.ExpireDate;
                return true;
            } else {
                return false;
            }
        }
    }
}
