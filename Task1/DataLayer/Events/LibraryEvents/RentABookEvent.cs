﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events {
    class RentABookEvent : Event {

        public bool Execute(IBookCatalog BookCatalog, Book Book, Reader Reader) {
            List<Book> Books = BookCatalog.Books;
            List<Rental> Rentals = BookCatalog.Rentals;
            if (isBookAvailableToRent(Books, Book)) {
                Rental rental = new Rental(Book, Reader);
                Rentals.Add(rental);
                Book.Quantity--;
                eventExecution(this);
                eventDescription = Reader.Name + " " + Reader.Surname + " rented " + Book.Title + " on " + rental.RentalDate;
                return true;
            }
            return false;
        }

        private bool isBookAvailableToRent(IList<Book> Books, Book book) {
            Console.WriteLine("was here");
            if (Books.Any(x => x.Id == book.Id) && book.Quantity > 0) {
                Console.WriteLine("and here");
                return true;
            }
            return false;
        }
    }
}
