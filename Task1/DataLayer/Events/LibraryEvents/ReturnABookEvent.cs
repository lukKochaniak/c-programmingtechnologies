﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events.LibraryEvents {
    class ReturnABookEvent : Event {
        public bool Execute(IBookCatalog BookCatalog, Book book) {
            List<Rental> Rentals = BookCatalog.Rentals;
            foreach (Rental rental in Rentals) {
                if (rental.Book == book) {
                    rental.isReturned = true;
                    book.Quantity++;
                    eventExecution(this);
                    eventDescription = rental.Reader.Name + " " + rental.Reader.Surname + " returned " + book.Title + " on " + System.DateTime.Now;
                    return true;
                }
            }
            return false;
        }
    }
}
