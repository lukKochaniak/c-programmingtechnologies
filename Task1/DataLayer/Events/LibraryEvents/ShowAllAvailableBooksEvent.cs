﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Events.LibraryEvents {
    class ShowAllAvailableBooksEvent : Event {
        public List<Book> Execute(IBookCatalog BookCatalog) {
            List<Rental> Rentals = BookCatalog.Rentals;
            List<Book> Books = BookCatalog.Books;
            List<Book> availableBooks = new List<Book>();
            foreach (Book book in Books) {
                if (isBookAvailableToRent(Books, Rentals, book)) {
                    availableBooks.Add(book);
                }
            }
            eventExecution(this);
            eventDescription = "Showed all available books";
            return availableBooks;
        }

        private bool isBookAvailableToRent(IList<Book> Books, IList<Rental> Rentals, Book book) {
            if (Books.Any(x => x.Id == book.Id)) {
                bool rented = false;
                foreach (Rental rental in Rentals) {
                    if (book.Id == rental.Book.Id && !rental.isReturned)
                        rented = true;
                }
                return !rented;
            }
            return true;
        }
    }
}
