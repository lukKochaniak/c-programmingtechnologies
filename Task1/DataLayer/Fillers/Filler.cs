﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DataLayer {
    public class Filler : IFiller {
        public int numberOfAutofilledRecords;

        public Filler(int numberOfAutofilledRecords) {
            this.numberOfAutofilledRecords = numberOfAutofilledRecords;
        }
        //TODO: Check it later
        public void FillBooks(Dictionary<int, Book> Books, List<Writer> Writers) {
            for (int i = 0; i < numberOfAutofilledRecords; i++) {
                Books.Add(i, new Book(i, "Title " + i, Writers[0], i * 10, 1990 + i));
            }
        }

        public void FillReaders(List<Reader> Readers) {
            for (int i = 0; i < numberOfAutofilledRecords; i++) {
                Readers.Add(new Reader("Name" + i, "Surname" + i, "City" + i, "Street" + i, "StreetNumber" + i, "FlatNumber" + i));
            }
        }

        public void FillRentals(ObservableCollection<Rental> Rentals, List<Reader> Readers, Dictionary<int, Book> Books) {
            for (int i = 0; i < numberOfAutofilledRecords; i++) {
                Rentals.Add(new Rental(Books[i], Readers[i], new DateTime(1999, 01, 01), new DateTime(1999, 01, 20)));
            }
        }

        public void FillWriters(List<Writer> Writers) {
            for (int i = 0; i < numberOfAutofilledRecords; i++) {
                Writers.Add(new Writer("Name" + i, "Surname" + i));
            }
        }


    }
}
