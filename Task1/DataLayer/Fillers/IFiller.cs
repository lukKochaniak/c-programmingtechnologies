﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DataLayer {
    public interface IFiller {
        void FillBooks(Dictionary<int, Book> Books, List<Writer> Writers);
        void FillReaders(List<Reader> Readers);
        void FillRentals(ObservableCollection<Rental> Rentals, List<Reader> Readers, Dictionary<int, Book> Books);
        void FillWriters(List<Writer> Writers);
    }
}
