﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer {
    public interface IBookCatalog {
        List<Book> Books { get; set; }
        List<Reader> Readers { get; set; }
        List<Rental> Rentals { get; set; }

    }
}
