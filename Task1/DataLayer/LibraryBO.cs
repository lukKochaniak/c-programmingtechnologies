﻿using DataLayer.Events;
using DataLayer.Events.LibraryEvents;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataLayer {
    public class LibraryBO {
        private IBookCatalog BookCatalog;
        private List<Event> Events;

        public LibraryBO() {
            BookCatalog = new BookCatalog();
            Events = new List<Event>();
        }

        public LibraryBO(IBookCatalog BookCatalog) {
            this.BookCatalog = BookCatalog;
            this.Events = new List<Event>();
        }

        public List<Book> returnBooks() {
            return BookCatalog.Books;
        }

        public List<Reader> returnReaders() {
            return BookCatalog.Readers;
        }

        public List<Rental> returnRentals() {
            return BookCatalog.Rentals;
        }

        public List<string> getEventsDescription() {
            List<string> logs = new List<string>();
            foreach (Event ev in Events) {
                logs.Add(ev.eventDescription);
            }
            return logs;
        }

        public List<Event> getEvents()
        {
            return Events;
        }

        private void LogAfterEvent(Event addEvent) {
            Events.Add(addEvent);
        }

        public bool addBook(Book Book) {
            AddBookEvent addBookEvent = new AddBookEvent();
            addBookEvent.afterEventExecution += LogAfterEvent;
            return addBookEvent.Execute(BookCatalog, Book);
        }


        public bool addReader(Reader Reader) {
            AddReaderEvent addReaderEvent = new AddReaderEvent();
            addReaderEvent.afterEventExecution += LogAfterEvent;
            return addReaderEvent.Execute(BookCatalog, Reader);
        }

        public bool addRental(Rental Rental) {
            AddRentalEvent addRentalEvent = new AddRentalEvent();
            addRentalEvent.afterEventExecution += LogAfterEvent;
            return addRentalEvent.Execute(BookCatalog, Rental);
        }

        public bool removeBook(Book Book) {
            RemoveBookEvent removeBookEvent = new RemoveBookEvent();
            removeBookEvent.afterEventExecution += LogAfterEvent;
            return removeBookEvent.Execute(BookCatalog, Book);
        }

        public bool removeReader(Reader Reader) {
            RemoveReaderEvent removeReaderEvent = new RemoveReaderEvent();
            removeReaderEvent.afterEventExecution += LogAfterEvent;
            return removeReaderEvent.Execute(BookCatalog, Reader);
        }

        public bool removeRental(Rental Rental) {
            RemoveRentalEvent removeRentalEvent = new RemoveRentalEvent();
            removeRentalEvent.afterEventExecution += LogAfterEvent;
            return removeRentalEvent.Execute(BookCatalog, Rental);
        }

        public bool renewRental(Rental Rental, DateTime RenewTo) {
            RenewRentalEvent renewRentalEvent = new RenewRentalEvent();
            renewRentalEvent.afterEventExecution += LogAfterEvent;
            return renewRentalEvent.Execute(BookCatalog, Rental, RenewTo);
        }

        public bool rentABook(Book Book, Reader Reader) {
            RentABookEvent rentABookEvent = new RentABookEvent();
            rentABookEvent.afterEventExecution += LogAfterEvent;
            return rentABookEvent.Execute(BookCatalog, Book, Reader);
        }

        public bool returnABook(Book book) {
            ReturnABookEvent returnABookEvent = new ReturnABookEvent();
            returnABookEvent.afterEventExecution += LogAfterEvent;
            return returnABookEvent.Execute(BookCatalog, book);
        }

        public List<Book> showAllAvailableBooks() {
            ShowAllAvailableBooksEvent showAllAvailableBooksEvent = new ShowAllAvailableBooksEvent();
            showAllAvailableBooksEvent.afterEventExecution += LogAfterEvent;
            return showAllAvailableBooksEvent.Execute(BookCatalog);
        }

        public void ReadBooksFromFile(string path) {
            using (StreamReader myFile = new StreamReader(path)) {
                while (!myFile.EndOfStream) {
                    string line = myFile.ReadLine();
                    string[] expressions = line.Split(',');
                    Book book = new Book(Int32.Parse(expressions[0]), expressions[1], new Writer(expressions[2], expressions[3]), Int32.Parse(expressions[4]), Int32.Parse(expressions[5]));
                    addBook(book);
                }
            }
        }

        public void ReadReadersFromFile(string path) {
            using (StreamReader myFile = new StreamReader(path)) {
                while (!myFile.EndOfStream) {
                    string line = myFile.ReadLine();
                    string[] expressions = line.Split(',');
                    Reader reader = new Reader(expressions[0], expressions[1], expressions[2], expressions[3], expressions[4], expressions[5]);
                    addReader(reader);
                }
            }
        }

        public void ReadRentalsFromFile(string path) {
            using (StreamReader myFile = new StreamReader(path)) {
                while (!myFile.EndOfStream) {
                    string line = myFile.ReadLine();
                    string[] expressions = line.Split(',');
                    Rental rental = new Rental(new Book(Int32.Parse(expressions[0]), expressions[1], new Writer(expressions[2], expressions[3]), Int32.Parse(expressions[4]), Int32.Parse(expressions[5])),
                         new Reader(expressions[6], expressions[7], expressions[8], expressions[9], expressions[10], expressions[11]), new DateTime(1990, 10, 10));
                    addRental(rental);
                }
            }
        }
    }
}
