﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataLayer;

namespace DataLayerUnitTests {
    [TestClass]
    public class BookTest {
        [TestMethod]
        public void BookConstructorTest() {
            Writer testWriter = new Writer("Name", "Surname");
            Book testBook = new Book(1, "Title", testWriter, 10, 1990);

            Assert.AreEqual(1, testBook.Id);
            Assert.AreEqual("Title", testBook.Title);
            Assert.AreEqual(testWriter, testBook.Author);
            Assert.AreEqual(10, testBook.Pages);
            Assert.AreEqual(1990, testBook.Year);
            }
        }
    }
