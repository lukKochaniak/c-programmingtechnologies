﻿using DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataLayerUnitTests {
    [TestClass]
    public class LibraryBOTest {
        private LibraryBO testLibraryBO;
        private Writer testWriter;
        private Book testBook;
        private Reader testReader;
        private IBookCatalog BookCatalog;

        [TestInitialize]
        public void Setup() {
            BookCatalog = new TestBookCatalog();
            testLibraryBO = new LibraryBO(BookCatalog);
            testWriter = new Writer("Name", "Surname");
            testReader = new Reader("Name", "Surname", "City", "Street", "StreetNumber", "FlatNumber");
            testBook = new Book(4, "Title", testWriter, 10, 1990);
        }


        [TestMethod]
        public void LibraryBOAddBookTest() {
            //given
            testLibraryBO = new LibraryBO();
            Assert.AreEqual(testLibraryBO.returnBooks().Count, 0);

            //when
            Assert.IsTrue(testLibraryBO.addBook(testBook));

            //then
            Assert.AreEqual(testLibraryBO.returnBooks().Count, 1);
            //Console.WriteLine(testLibraryBO.getEventsDescription()[0]);
        }

        [TestMethod]
        public void LibraryBOAddReaderTest() {
            //given
            testLibraryBO = new LibraryBO();
            Assert.AreEqual(testLibraryBO.returnReaders().Count, 0);

            //when
            Assert.IsTrue(testLibraryBO.addReader(testReader));

            //then
            Assert.AreEqual(testLibraryBO.returnReaders().Count, 1);
        }

        [TestMethod]
        public void LibraryBOAddRentalTest() {
            //given
            DateTime testRentalDate = new DateTime(2014, 01, 01);
            DateTime testExpireDate = new DateTime(2014, 01, 06);
            Rental testRental = new Rental(testBook, testReader, testRentalDate, testExpireDate);
            testLibraryBO = new LibraryBO();
            Assert.AreEqual(testLibraryBO.returnRentals().Count, 0);

            //when
            Assert.IsTrue(testLibraryBO.addRental(testRental));

            //then
            Assert.AreEqual(testLibraryBO.returnRentals().Count, 1);
        }

        [TestMethod]
        public void LibraryBORemoveBookTest() {
            //given
            var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\testFiles\\books.txt");
            testLibraryBO.ReadBooksFromFile(path);
            BookCatalog.Books = testLibraryBO.returnBooks();
            testLibraryBO = new LibraryBO(BookCatalog);
            List<Book> Books = testLibraryBO.returnBooks();
            int currentNumberOfBooks = testLibraryBO.returnBooks().Count;

            //when
            Assert.IsTrue(testLibraryBO.removeBook(Books[currentNumberOfBooks - 1]));

            //then
            Assert.AreEqual(currentNumberOfBooks - 1, testLibraryBO.returnBooks().Count);
        }

        [TestMethod]
        public void LibraryBORemoveReaderTest() {
            //given
            var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\testFiles\\readers.txt");
            testLibraryBO.ReadReadersFromFile(path);
            List<Reader> Readers = testLibraryBO.returnReaders();
            int currentNumberOfReaders = testLibraryBO.returnReaders().Count;

            //when
            Assert.IsTrue(testLibraryBO.removeReader(Readers[currentNumberOfReaders - 1]));

            //then
            Assert.AreEqual(currentNumberOfReaders - 1, testLibraryBO.returnReaders().Count);
        }

        [TestMethod]
        public void LibraryBORemoveRentalTest() {
            //given
            var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\testFiles\\rentals.txt");
            testLibraryBO.ReadRentalsFromFile(path);
            List<Rental> Rentals = testLibraryBO.returnRentals();
            int currentNumberOfRentals = testLibraryBO.returnRentals().Count;

            //when
            Assert.IsTrue(testLibraryBO.removeRental(Rentals[currentNumberOfRentals - 1]));

            //then
            Assert.AreEqual(currentNumberOfRentals - 1, testLibraryBO.returnRentals().Count);
        }

        [TestMethod]
        public void LibraryBORenewRentalWithDateGivenTest() {
            //given
            DateTime testRentalDate = new DateTime(2014, 01, 01);
            DateTime testExpireDate = new DateTime(2014, 01, 06);
            DateTime testExpireDateRenewed = new DateTime(2014, 01, 20);
            Rental testRental = new Rental(testBook, testReader, testRentalDate, testExpireDate);
            testLibraryBO = new LibraryBO();

            //when
            testLibraryBO.addRental(testRental);

            //then
            List<Rental> Rentals = testLibraryBO.returnRentals();
            Assert.AreEqual(testExpireDate, Rentals[0].ExpireDate);
            Assert.IsTrue(testLibraryBO.renewRental(Rentals[0], testExpireDateRenewed));
            Assert.AreEqual(testExpireDateRenewed, Rentals[0].ExpireDate);
        }

        [TestMethod]
        public void LibraryBORentABookTest() {
            //given
            testLibraryBO = new LibraryBO(BookCatalog);
            testLibraryBO.addBook(testBook);

            //when
            Assert.IsTrue(testLibraryBO.rentABook(testBook, testReader));

            //then
            Assert.AreEqual(1, testLibraryBO.returnRentals().Count);
        }

        [TestMethod]
        public void LibraryBOReturnABookTest() {
            //given
            testLibraryBO = new LibraryBO(BookCatalog);
            testLibraryBO.addBook(testBook);
            testLibraryBO.rentABook(testBook, testReader);

            //when
            Assert.IsTrue(testLibraryBO.returnABook(testBook));

            //then
            Assert.IsTrue(testLibraryBO.returnRentals()[0].isReturned);
        }

        [TestMethod]
        public void LibraryBOShowAvailableBooksTest() {
            //given
            testLibraryBO = new LibraryBO(BookCatalog);

            //when
            List<Book> result = testLibraryBO.showAllAvailableBooks();

            //then
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public void LibraryBOBookQuantityTest() {
            //given
            testLibraryBO.addBook(testBook);
            testLibraryBO.addBook(testBook);
            testLibraryBO.addBook(testBook);

            //when
            Assert.IsTrue(testLibraryBO.rentABook(testBook, testReader));

            //then
            Assert.AreEqual(2, testBook.Quantity);
        }
    }
}
