﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataLayer;

namespace DataLayerUnitTests {
    [TestClass]
    public class RentalTest {
        private Writer testAuthor;
        private Book testBook;
        private Reader testReader;
        private DateTime testRentalDate;
        private DateTime testExpireDate;

        [TestInitialize]
        public void Setup() {
            testAuthor = new Writer("Name", "Surname");
            testBook = new Book(1, "Title", testAuthor, 10, 1990);
            testReader = new Reader("Name", "Surname", "City", "Street", "StreetNumber", "FlatNumber");
            testRentalDate = new DateTime(2014, 01, 01);
            testExpireDate = new DateTime(2014, 01, 15);
            }

        [TestMethod]
        public void RentalConstructorWithRentalDateTest() {
            Rental testRental = new Rental(testBook, testReader, testRentalDate);

            Assert.AreEqual(testRental.Reader, testReader);
            Assert.AreEqual(testRental.Book, testBook);
            Assert.AreEqual(testRental.RentalDate, testRentalDate);
            Assert.AreEqual(testRental.ExpireDate, testExpireDate);
            }

        [TestMethod]
        public void RentalConstructorWithRentalAndExpireDateTest() {
            Rental testRental = new Rental(testBook, testReader, testRentalDate, testExpireDate);

            Assert.AreEqual(testRental.Reader, testReader);
            Assert.AreEqual(testRental.Book, testBook);
            Assert.AreEqual(testRental.RentalDate, testRentalDate);
            Assert.AreEqual(testRental.ExpireDate, testExpireDate);
            }
        }
    }
