﻿using DataLayer;
using System;
using System.Collections.Generic;

namespace DataLayerUnitTests {
    class TestBookCatalog : IBookCatalog {
        public List<Book> Books { get; set; }
        public List<Reader> Readers { get; set; }
        public List<Rental> Rentals { get; set; }

        public TestBookCatalog() {
            this.Rentals = new List<Rental>();
            this.Books = fillBooks();
            this.Readers = fillReaders();
        }

        public TestBookCatalog(List<Book> books, List<Reader> readers, List<Rental> rentals) {
            Books = books;
            Readers = readers;
            Rentals = rentals;
        }

        private List<Rental> fillRentals() {
            List<Rental> filledRentals = new List<Rental>();
            filledRentals.Add(new Rental(new Book(1, "testTitleOne", new Writer("Name", "Surname"), 100, 1990), new Reader("nameOne", "surnameOne", "City", "Street", "StreetNumber", "FlatNumber"), new DateTime(1990, 10, 10)));
            filledRentals.Add(new Rental(new Book(2, "testTitleTwo", new Writer("Name", "Surname"), 100, 1990), new Reader("nameTwo", "surnameTwo", "City", "Street", "StreetNumber", "FlatNumber"), new DateTime(1990, 10, 10)));
            filledRentals.Add(new Rental(new Book(3, "testTitleThree", new Writer("Name", "Surname"), 100, 1990), new Reader("nameThree", "surnameThree", "City", "Street", "StreetNumber", "FlatNumber"), new DateTime(1990, 10, 10)));

            return filledRentals;
        }

        private List<Book> fillBooks() {
            List<Book> filledBooks = new List<Book>();
            filledBooks.Add(new Book(1, "testTitleOne", new Writer("Name", "Surname"), 100, 1990));
            filledBooks.Add(new Book(2, "testTitleTwo", new Writer("Name", "Surname"), 100, 1990));
            filledBooks.Add(new Book(3, "testTitleThree", new Writer("Name", "Surname"), 100, 1990));

            foreach(Book book in filledBooks) {
                book.Quantity++;
            }

            return filledBooks;
        }

        private List<Reader> fillReaders() {
            List<Reader> filledReaders = new List<Reader>();
            filledReaders.Add(new Reader("nameOne", "surnameOne", "City", "Street", "StreetNumber", "FlatNumber"));
            filledReaders.Add(new Reader("nameTwo", "surnameTwo", "City", "Street", "StreetNumber", "FlatNumber"));
            filledReaders.Add(new Reader("nameThree", "surnameThree", "City", "Street", "StreetNumber", "FlatNumber"));

            return filledReaders;
        }
    }
}
