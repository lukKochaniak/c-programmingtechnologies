﻿using DataLayer;
using System;
using System.Collections.Generic;

namespace LogicLayer {
    public class UserController {
        private LibraryBO libraryBO;

        public UserController() {
            libraryBO = new LibraryBO();
        }

        public UserController(LibraryBO libraryBO) {
            this.libraryBO = libraryBO;
        }
        
        public bool rentABook(Book book, Reader reader) {
            return libraryBO.rentABook(book, reader);
        }

        public bool returnABook(Book book) {
            return libraryBO.returnABook(book);
        }

        public bool addBook(Book book) {
            return libraryBO.addBook(book);
        }

        public bool addBook(Reader reader) {
            return libraryBO.addReader(reader);
        }

        public bool addRental(Rental rental) {
            return libraryBO.addRental(rental);
        }

        public bool removeBook(Book book) {
            return libraryBO.removeBook(book);
        }

        public bool removeReader(Reader Reader) {
            return libraryBO.removeReader(Reader);
        }

        public bool removeRental(Rental rental) {
            return libraryBO.removeRental(rental);
        }

        public bool renewRental(Rental rental, DateTime renewTo) {
            return libraryBO.renewRental(rental, renewTo);
        }

        public List<Book> showAllAvailableBooks() {
            return libraryBO.showAllAvailableBooks();
        }


    }
}
