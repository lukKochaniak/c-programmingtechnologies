﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer {
    public partial class DatabaseDataContext {

        public void AddABook(Book instance) {
            if (Books.ToList().Count == 0) {
                instance.Id = 1;
            } else
                instance.Id = Books.ToList()[Books.ToList().Count-1].Id + 1;
            Books.InsertOnSubmit(instance);
            SubmitChanges();
        }

        public void DeleteABook(int Id) {
            IEnumerable query = from item in Books
                        where item.Id == Id
                        select item;
            foreach (Book book in query)
                this.Books.DeleteOnSubmit(book);
            this.SubmitChanges();
        }

        public void CleanBooks() {
            IEnumerable query = from item in Books
                        select item;

            foreach (Book book in query) {
                Books.DeleteOnSubmit(book);
                SubmitChanges();
            }
        }

        public void UpdateABook(Book instance) {
            IEnumerable query = from item in Books
                        where item.Id == instance.Id
                        select item;


            foreach (Book book in query) {
                book.Id = instance.Id;
                book.AuthorKey = instance.AuthorKey;
                book.Pages = instance.Pages;
                book.Quantity = instance.Quantity;
                book.Title = instance.Title;
                book.Year = instance.Year;
            }
            SubmitChanges();
        }

        public void AddAReader(Reader instance) {
            if (Readers.ToList().Count == 0) {
                instance.Id = 1;
            } else
                instance.Id = Readers.ToList()[Readers.ToList().Count-1].Id + 1;
            Readers.InsertOnSubmit(instance);
            SubmitChanges();
        }

        public void DeleteAReader(int Id) {
            IEnumerable query = from item in Readers
                        where item.Id == Id
                        select item;
            foreach (Reader reader in query)
                Readers.DeleteOnSubmit(reader);
            SubmitChanges();
        }

        public void CleanReaders() {
            IEnumerable query = from item in Readers
                        select item;
                        
            foreach(Reader reader in query) {
                Readers.DeleteOnSubmit(reader);
                SubmitChanges();
            }
        }

        public void UpdateAReader(Reader instance) {
            IEnumerable query = from item in Readers
                        where item.Id == instance.Id
                        select item;


            foreach (Reader reader in query) {
                reader.Id = instance.Id;
                reader.Name = instance.Name;
                reader.Street = instance.Street;
                reader.City = instance.City;
                reader.Surname = instance.Surname;
                reader.StreetNumber = instance.StreetNumber;
            }

            SubmitChanges();
        }

        public void AddAWriter(Writer instance) {
            if (Writers.ToList().Count == 0) {
                instance.Id = 1;
            } else
                instance.Id = Writers.ToList()[Writers.ToList().Count-1].Id + 1;
            Writers.InsertOnSubmit(instance);
            SubmitChanges();
        }

        public void DeleteAWriter(int Id) {
            IEnumerable query = from item in Writers
                        where item.Id == Id
                        select item;
            foreach (Writer writer in query)
                Writers.DeleteOnSubmit(writer);
            SubmitChanges();
        }

        public void CleanWriters() {
            IEnumerable query = from item in Writers
                        select item;

            foreach (Writer writer in query) {
                Writers.DeleteOnSubmit(writer);
                SubmitChanges();
            }
        }

        public void UpdateAWriter(Writer instance) {
            IEnumerable query = from item in Writers
                        where item.Id == instance.Id
                        select item;


            foreach (Writer writer in query) {
                writer.Id = instance.Id;
                writer.Name = instance.Name;
                writer.Surname = instance.Surname;
            }

            SubmitChanges();
        }

        public void AddARental(Rental instance) {
            if(Rentals.ToList().Count == 0) {
                instance.Id = 1;
            }
            else
                instance.Id = Rentals.ToList()[Rentals.ToList().Count-1].Id + 1;
            Rentals.InsertOnSubmit(instance);
            SubmitChanges();
        }

        public void DeleteARental(int Id) {
            IEnumerable query = from item in Rentals
                        where item.Id == Id
                        select item;
            foreach (Rental rental in query)
                Rentals.DeleteOnSubmit(rental);
            SubmitChanges();
        }

        public void CleanRentals() {
            IEnumerable query = from item in Rentals
                        select item;

            foreach (Rental rental in query) {
                Rentals.DeleteOnSubmit(rental);
                SubmitChanges();
            }
        }

        public void UpdateARental(Rental instance) {
            IEnumerable query = from item in Rentals
                        where item.Id == instance.Id
                        select item;


            foreach (Rental rental in query) {
                rental.Id = instance.Id;
                rental.ExpireDate = instance.ExpireDate;
                rental.RentalDate = instance.RentalDate;
                rental.BookKey = instance.BookKey;
                rental.ReaderKey = instance.ReaderKey;
            }

            SubmitChanges();
        }


        public List<Book> GetBooks() {
            return Books.ToList();
        }

        public List<Rental> GetRentals() {
            return Rentals.ToList();
        }

        public List<Writer> GetWriters() {
            return Writers.ToList();
        }

        public List<Reader> GetReaders() {
            return Readers.ToList();
        }

        public void CleanEverythingInDB() {
            CleanRentals();
            CleanBooks();
            CleanWriters();
            CleanReaders();
        }
    }
}
