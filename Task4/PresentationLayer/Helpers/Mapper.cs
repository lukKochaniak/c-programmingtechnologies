﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicesLayer;

namespace PresentationLayer {
    public class Mapper {
        public static Book mapBookFromModel(MBook Book) {
            return new Book(Book.Id, Book.Title, mapWriterFromModel(Book.Author), Int32.Parse(Book.Pages), Int32.Parse(Book.Year), Book.Quantity);
        }

        public static MBook mapBookToModel(Book Book) {
            return new MBook(Book.Id, Book.Title, mapWriterToModel(Book.Author), Book.Pages.ToString(), Book.Year.ToString(), Book.Quantity);
        }

        public static List<MBook> mapBooksToModel(List<Book> Books) {
            List<MBook> MBooks = new List<MBook>();
            foreach(Book book in Books){
                MBooks.Add(mapBookToModel(book));
            }
            return MBooks;
        }

        public static Writer mapWriterFromModel(MWriter Writer) {
            return new Writer(Writer.Id, Writer.Name, Writer.Surname);
        }

        public static MWriter mapWriterToModel(Writer Writer) {
            return new MWriter(Writer.Id, Writer.Name, Writer.Surname);
        }

        public static List<MWriter> mapWritersToModel(List<Writer> Writers) {
            List<MWriter> MWriters = new List<MWriter>();
            foreach(Writer writer in Writers) {
                MWriters.Add(mapWriterToModel(writer));
            }
            return MWriters;
        }

        public static Reader mapReaderFromModel(MReader Reader) {
            return new Reader(Reader.Id, Reader.Name, Reader.Surname, Reader.City, Reader.Street, Reader.StreetNumber, Reader.FlatNumber);
        }

        public static MReader mapReaderToModel(Reader Reader) {
            return new MReader(Reader.Id, Reader.Name, Reader.Surname, Reader.City, Reader.Street, Reader.StreetNumber, Reader.FlatNumber);
        }

        public static List<MReader> mapReadersToModel(List<Reader> Readers) {
            List<MReader> MReaders = new List<MReader>();
            foreach(Reader reader in Readers) {
                MReaders.Add(mapReaderToModel(reader));
            }
            return MReaders;
        }

        public static Rental mapRentalFromModel(MRental Rental) {
            return new Rental(Rental.Id, mapBookFromModel(Rental.Book), mapReaderFromModel(Rental.Reader), Rental.RentalDate, Rental.ExpireDate);
        }

        public static MRental mapRentalToModel(Rental Rental) {
            return new MRental(Rental.Id, mapBookToModel(Rental.Book), mapReaderToModel(Rental.Reader), Rental.RentalDate, Rental.ExpireDate);
        }

        public static List<MRental> mapRentalsToModel(List<Rental> Rentals) {
            List<MRental> MRentals = new List<MRental>();
            foreach(Rental rental in Rentals) {
                MRentals.Add(mapRentalToModel(rental));
            }
            return MRentals;
        }
    }
}
