﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer {
    [Serializable]
    public class MBook : ValidatableModel{
        public int Id { set; get; }
        [Required(ErrorMessage = "A Title is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string Title { set; get; }
        [Required(ErrorMessage = "An author is requied")]
        public MWriter Author { set; get; }
        [Required(ErrorMessage = "Pages requied")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "This field must be a number")]
        public string Pages { set; get; }
        [Required(ErrorMessage = "Year requied")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "This field must be a number")]
        public string Year { set; get; }
        public int Quantity { set; get; }

        public MBook(int Id, string Title, MWriter Author, string Pages, string Year, int Quantity) {
            this.Id = Id;
            this.Title = Title;
            this.Pages = Pages;
            this.Year = Year;
            this.Author = Author;
            this.Quantity = Quantity;
        }

        public override string ToString() {
            return Title;
        }

    }
}
