﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer {
    [Serializable]
    public class MReader : ValidatableModel {
        public int Id { set; get; }
        [Required(ErrorMessage = "A name is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string Name { get; set; }
        [Required(ErrorMessage = "A surname is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "A city is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string City { get; set; }
        [Required(ErrorMessage = "A street is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string Street { get; set; }
        [Required(ErrorMessage = "A streetnumber is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "This field must be a number")]
        public string StreetNumber { get; set; }
        [Required(ErrorMessage = "A flatnumber is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "This field must be a number")]
        public string FlatNumber { get; set; }

        public MReader(int Id, string Name, string Surname, string City, string Street, string StreetNumber, string FlatNumber) {
            this.Id = Id;
            this.Name = Name;
            this.Surname = Surname;
            this.City = City;
            this.Street = Street;
            this.StreetNumber = StreetNumber;
            this.FlatNumber = FlatNumber;
        }

        public override string ToString() {
            return Name + " " + Surname;
        }
    }
}
