﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer {
    [Serializable]
    public class MRental : ValidatableModel {
        public const int DAYS_OF_RENTAL = 14;
        public int Id { set; get; }
        [Required(ErrorMessage = "A Book is requied")]
        public MBook Book { set; get; }
        [Required(ErrorMessage = "A Reader is requied")]
        public MReader Reader { set; get; }
        public DateTime RentalDate { set; get; }
        public DateTime ExpireDate { set; get; }
        public bool isReturned { set; get; }

        public MRental(int Id, MBook Book, MReader Reader) {
            this.Id = Id;
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = System.DateTime.Now;
            this.ExpireDate = RentalDate.AddDays(DAYS_OF_RENTAL);
            isReturned = false;
        }
        public MRental(int Id, MBook Book, MReader Reader, DateTime RentalDate) {
            this.Id = Id;
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = RentalDate;
            this.ExpireDate = RentalDate.AddDays(DAYS_OF_RENTAL);
            isReturned = false;
        }

        public MRental(int Id, MBook Book, MReader Reader, DateTime RentalDate, DateTime ExpireDate) {
            this.Id = Id;
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = RentalDate;
            this.ExpireDate = ExpireDate;
            isReturned = false;
        }

    }
}
