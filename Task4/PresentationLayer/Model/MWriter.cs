﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer {
    [Serializable]
    public class MWriter : ValidatableModel {
        public int Id { set; get; }
        [Required(ErrorMessage = "A name is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string Name { set; get; }
        [Required(ErrorMessage = "A surname is requied")]
        [StringLength(50, ErrorMessage = "Exceeded character limit")]
        public string Surname { set; get; }

        public MWriter(int Id, string Name, string Surname) {
            this.Id = Id;
            this.Name = Name;
            this.Surname = Surname;
        }

        public override string ToString() {
            return Name + " " + Surname;
        }
    }
}
