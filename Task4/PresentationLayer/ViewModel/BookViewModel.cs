﻿using ServicesLayer;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Linq;

namespace PresentationLayer {
    public class BookViewModel : MainViewModel {
        #region Constructor
        public BookViewModel() {
            cAddBook = new RelayCommand(addBook);
            cEditBook = new RelayCommand(editBook);
            cDeleteBook = new RelayCommand(deleteBook);
        }
        #endregion

        #region Commands
        public ICommand cAddBook { get; set; }
        public ICommand cEditBook { get; set; }
        public ICommand cDeleteBook { get; set; }
        #endregion

        #region Selected Item
        private static MBook _SelectedBook = new MBook(0, "", new MWriter(0, "", ""), "", "", 0);
        public MBook SelectedBook {
            get { return _SelectedBook; }
            set { _SelectedBook = value; RaisePropertyChanged("SelectedBook"); }
        }

        //public MWriter SelectedWriter {
        //    get { return WriterList.Where(x => x.Id == SelectedBook.Author.Id).First(); }
        //    set { SelectedWriter = value; RaisePropertyChanged("SelectedWriter"); }
        //}
        #endregion

        #region Clean Item
        private MBook _ClearBook = new MBook(0, "", null, "", "", 0);
        public MBook ClearBook {
            get { return _ClearBook; }
            set { _ClearBook = value; }
        }
        #endregion

        #region Actions
        public void addBook(object obj) {
            if (SelectedBook != null) {
                SelectedBook.Validate();
                if (SelectedBook.HasErrors || BookList.Contains(SelectedBook) || SelectedBook.Author.Name == "" || SelectedBook.Pages == "" || SelectedBook.Title == "" || SelectedBook.Year == "") {
                    //ClearBook = new MBook(0, "", new MWriter(0, "", ""), 0, 0);
                    //SelectedBook = ClearBook;
                } else {
                    Console.WriteLine("Book: " + SelectedBook.Id + " " + SelectedBook.Title + " " + SelectedBook.Author.Id);
                    UserController.addBook(Mapper.mapBookFromModel(SelectedBook));
                    ClearBook = new MBook(0, "", new MWriter(0, "", ""), "", "", 0);
                    SelectedBook = ClearBook;
                    RaisePropertyChanged("BookList");
                }
            } else {
                ClearBook = new MBook(0, "", new MWriter(0, "", ""), "", "", 0);
                SelectedBook = ClearBook;
            }
        }

        public void editBook(object obj) {
            SelectedBook.Validate();
            if (!SelectedBook.HasErrors) {
                UserController.editBook(Mapper.mapBookFromModel(SelectedBook));
                ClearBook = new MBook(0, "", new MWriter(0, "", ""), "", "", 0);
                SelectedBook = ClearBook;
                RaisePropertyChanged("BookList");
            }
        }

        public void deleteBook(object obj) {
            if (!RentalList.Any(x => x.Book.Id == SelectedBook.Id)) {
                UserController.removeBook(Mapper.mapBookFromModel(SelectedBook));
                ClearBook = new MBook(0, "", new MWriter(0, "", ""), "", "", 0);
                SelectedBook = ClearBook;
                RaisePropertyChanged("BookList");
            }
        }
        #endregion
    }
}
