﻿using ServicesLayer;
using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace PresentationLayer {
    public class MainViewModel : INotifyPropertyChanged {
        UserController userController;

        #region Properties Change
        internal void RaisePropertyChanged(string prop) {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructor
        public MainViewModel() {
            cShowBooks = new RelayCommand(showBooksWindow);
            cShowWriters = new RelayCommand(showWritersWindow);
            cShowRentals = new RelayCommand(showRentalsWindow);
            cShowReaders = new RelayCommand(showReadersWindow);

            userController = new UserController();
        }
        #endregion

        #region Container and Lists
        public static LibraryBO libraryBO = new LibraryBO();
        public ObservableCollection<MBook> BookList {
            get { return new ObservableCollection<MBook>(Mapper.mapBooksToModel(userController.getBooks())); }
        }
        public ObservableCollection<MReader> ReaderList {
            get { return new ObservableCollection<MReader>(Mapper.mapReadersToModel(userController.getReaders())); }
        }
        public ObservableCollection<MRental> RentalList {
            get { return new ObservableCollection<MRental>(Mapper.mapRentalsToModel(userController.getRentals())); }
        }
        public ObservableCollection<MWriter> WriterList {
            get { return new ObservableCollection<MWriter>(Mapper.mapWritersToModel(userController.getWriters())); }
        }
        #endregion

        #region Commands
        public ICommand cShowBooks { get; set; }
        public ICommand cShowWriters { get; set; }
        public ICommand cShowRentals { get; set; }
        public ICommand cShowReaders { get; set; }
        public UserController UserController {
            get {
                return userController;
            } 
            set {
                userController = value;
                RaisePropertyChanged("UserController");
            }
        }

        #endregion

        #region Windows
        public void showWritersWindow(object obj) {
            WriterListWindow win = new WriterListWindow();
            win.Show();
        }

        public void showRentalsWindow(object obj) {
            RentalListWindow win = new RentalListWindow();
            win.Show();
        }

        public void showBooksWindow(object obj) {
            BookListWindow win = new BookListWindow();
            win.Show();
        }

        public void showReadersWindow(object obj) {
            ReaderListWindow win = new ReaderListWindow();
            win.Show();
        }
        #endregion

        /*private void Refresh() {
            ProductsCollection = model.ProductCollection;
            NotifyPropertyChanged("ProductsCollection");
        }*/
    }
}
