﻿using System;
using System.Linq;
using System.Windows.Input;
using ServicesLayer;

namespace PresentationLayer {
    public class ReaderViewModel : MainViewModel {
        #region Constructor
        public ReaderViewModel() {
            cAddReader = new RelayCommand(addReader);
            cEditReader = new RelayCommand(editReader);
            cDeleteReader = new RelayCommand(deleteReader);
        }
        #endregion

        #region Commands
        public ICommand cAddReader { get; set; }
        public ICommand cEditReader { get; set; }
        public ICommand cDeleteReader { get; set; }
        #endregion

        #region Selected Item

        private static MReader _SelectedReader = new MReader(0, "", "", "", "", "", "");
        public MReader SelectedReader {
            get { return _SelectedReader; }
            set { _SelectedReader = value; RaisePropertyChanged("SelectedReader");  Console.WriteLine(SelectedReader.Id + " " + SelectedReader.Name + " " + SelectedReader.StreetNumber); }
        }

        #endregion

        #region Clean Item
        private MReader _ClearReader = new MReader(0, "", "", "", "", "", "");
        public MReader ClearReader {
            get { return _ClearReader; }
            set { _ClearReader = value; }
        }
        #endregion

        #region Actions

        public void addReader(object obj) {
            if (SelectedReader != null) {
                SelectedReader.Validate();
                if (SelectedReader.HasErrors || ReaderList.Contains(SelectedReader) || SelectedReader.Name == "" || SelectedReader.Surname == "" || SelectedReader.City == "" || SelectedReader.Street == "") {
                    //ClearReader = new MReader(0, "", "", "", "", "", "");
                    //SelectedReader = ClearReader;
                } else {
                    UserController.addReader(Mapper.mapReaderFromModel(SelectedReader));
                    ClearReader = new MReader(0, "", "", "", "", "", "");
                    SelectedReader = ClearReader;
                    RaisePropertyChanged("ReaderList");
                }
            } else {
                ClearReader = new MReader(0, "", "", "", "", "", "");
                SelectedReader = ClearReader;
            }
        }

        public void editReader(object obj) {
            SelectedReader.Validate();
            if (!SelectedReader.HasErrors) {
                UserController.editReader(Mapper.mapReaderFromModel(SelectedReader));
                ClearReader = new MReader(0, "", "", "", "", "", "");
                SelectedReader = ClearReader;
                RaisePropertyChanged("ReaderList");
            }
        }

        public void deleteReader(object obj) {
            if (!RentalList.Any(x => x.Reader.Id == SelectedReader.Id)) {
                UserController.removeReader(Mapper.mapReaderFromModel(SelectedReader));
                ClearReader = new MReader(0, "", "", "", "", "", "");
                SelectedReader = ClearReader;
                RaisePropertyChanged("ReaderList");
            }
        }
        #endregion
    }
}
