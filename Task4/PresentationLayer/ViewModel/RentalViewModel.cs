﻿using System;
using System.Linq;
using System.Windows.Input;
using ServicesLayer;

namespace PresentationLayer {
    public class RentalViewModel : MainViewModel {
        #region Constructor
        public RentalViewModel() {
            cAddRental = new RelayCommand(addRental);
            cEditRental = new RelayCommand(editRental);
            cDeleteRental = new RelayCommand(deleteRental);
        }
        #endregion

        #region Commands
        public ICommand cAddRental { get; set; }
        public ICommand cEditRental { get; set; }
        public ICommand cDeleteRental { get; set; }
        #endregion

        #region Selected Item
        private static MRental _SelectedRental = new MRental(0, null, null, new DateTime(2000, 01, 01), new DateTime(2000, 01, 02));
        public MRental SelectedRental {
            get {  return _SelectedRental; }
            set { _SelectedRental = value; RaisePropertyChanged("SelectedRental"); }
        }
        #endregion

        #region Clean Item
        private MRental _ClearRental = new MRental(0, null, null, new DateTime(2000, 01, 01), new DateTime(2000, 01, 01));
        public MRental ClearRental {
            get { return _ClearRental; }
            set { _ClearRental = value; }
        }
        #endregion

        #region Actions
        public void addRental(object obj) {
            if (SelectedRental != null) {
                SelectedRental.Validate();
                if (SelectedRental.HasErrors || RentalList.Contains(SelectedRental) || SelectedRental.Reader == null || SelectedRental.Book == null) {
                    //ClearRental = new MRental(0, new MBook(0, "", new MWriter(0, "", ""), 0, 0), new MReader(0, "", "", "", "", "", ""), new DateTime(), new DateTime());
                    //SelectedRental = ClearRental;
                } else {
                    UserController.addRental(Mapper.mapRentalFromModel(SelectedRental));
                    ClearRental = new MRental(0, null, null, new DateTime(2000, 01, 01), new DateTime(2000, 01, 01));
                    SelectedRental = ClearRental;
                    RaisePropertyChanged("RentalList");
                }
            } else {
                ClearRental = new MRental(0, null, null, new DateTime(2000, 01, 01), new DateTime(2000, 01, 01));
                SelectedRental = ClearRental;
            }
        }

        public void editRental(object obj) {
            SelectedRental.Validate();
            if (!SelectedRental.HasErrors) {
                UserController.editRental(Mapper.mapRentalFromModel(SelectedRental));
                ClearRental = new MRental(0, null, null, new DateTime(2000, 01, 01), new DateTime(2000, 01, 01));
                SelectedRental = ClearRental;
                RaisePropertyChanged("RentalList");
            }
        }

        public void deleteRental(object obj) {
            UserController.removeRental(Mapper.mapRentalFromModel(SelectedRental));
            ClearRental = new MRental(0, null, null, new DateTime(2000, 01, 01), new DateTime(2000, 01, 01));
            SelectedRental = ClearRental;
            RaisePropertyChanged("RentalList");
        }
        #endregion
    }
}
