﻿using System;
using System.Linq;
using System.Windows.Input;
using ServicesLayer;

namespace PresentationLayer {
    public class WriterViewModel : MainViewModel {
        #region Constructor
        public WriterViewModel() {
            cAddWriter = new RelayCommand(addWriter);
            cEditWriter = new RelayCommand(editWriter);
            cDeleteWriter = new RelayCommand(deleteWriter);
        }
        #endregion

        #region Commands

        public ICommand cAddWriter { get; set; }
        public ICommand cEditWriter { get; set; }
        public ICommand cDeleteWriter { get; set; }

        #endregion

        #region Selected Item

        private static MWriter _SelectedWriter = new MWriter(0, "", "");
        public MWriter SelectedWriter {
            get { return _SelectedWriter; }
            set { _SelectedWriter = value; RaisePropertyChanged("SelectedWriter"); }
        }

        #endregion

        #region Clean Item
        private MWriter _ClearWriter = new MWriter(0, "", "");
        public MWriter ClearWriter {
            get { return _ClearWriter; }
            set { _ClearWriter = value; }
        }
        #endregion

        #region Actions
        public void addWriter(object obj) {
            if (SelectedWriter != null) {
                SelectedWriter.Validate();
                if (SelectedWriter.HasErrors || WriterList.Contains((SelectedWriter)) || SelectedWriter.Name == "" || SelectedWriter.Surname == "") {
                    //ClearWriter = new MWriter(0, "", "");
                    //SelectedWriter = ClearWriter;
                } else {
                    UserController.addWriter(Mapper.mapWriterFromModel(SelectedWriter));
                    ClearWriter = new MWriter(0, "", "");
                    SelectedWriter = ClearWriter;
                    RaisePropertyChanged("WriterList");
                }
            } else {
                ClearWriter = new MWriter(0, "", "");
                SelectedWriter = ClearWriter;
            }
        }

        public void editWriter(object obj) {
            SelectedWriter.Validate();
            if (!SelectedWriter.HasErrors) {
                UserController.editWriter(Mapper.mapWriterFromModel(SelectedWriter));
                ClearWriter = new MWriter(0, "", "");
                SelectedWriter = ClearWriter;
                RaisePropertyChanged("WriterList");
            }
        }

        public void deleteWriter(object obj) {
            if (!BookList.Any(x => x.Author.Id == SelectedWriter.Id)) {
                UserController.removeWriter(Mapper.mapWriterFromModel(SelectedWriter));
                ClearWriter = new MWriter(0, "", "");
                SelectedWriter = ClearWriter;
                RaisePropertyChanged("WriterList");
            }
        }
        #endregion

    }
}
