﻿using System;

namespace ServicesLayer {
    [Serializable]
    public class Reader {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string FlatNumber { get; set; }

        public Reader(string Name, string Surname, string City, string Street, string StreetNumber, string FlatNumber) {
            this.Name = Name;
            this.Surname = Surname;
            this.City = City;
            this.Street = Street;
            this.StreetNumber = StreetNumber;
            this.FlatNumber = FlatNumber;
        }

        public Reader(int Id, string Name, string Surname, string City, string Street, string StreetNumber, string FlatNumber) {
            this.Id = Id;
            this.Name = Name;
            this.Surname = Surname;
            this.City = City;
            this.Street = Street;
            this.StreetNumber = StreetNumber;
            this.FlatNumber = FlatNumber;
        }

        public override string ToString() {
            return Name + " " + Surname;
        }
    }
}
