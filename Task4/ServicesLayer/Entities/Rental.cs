﻿using System;

namespace ServicesLayer {
    [Serializable]
    public class Rental {
        public const int DAYS_OF_RENTAL = 14;
        public int Id { set; get; }
        public Book Book { set; get; }
        public Reader Reader { set; get; }
        public DateTime RentalDate { set; get; }
        public DateTime ExpireDate { set; get; }
        public bool isReturned { set; get; }

        public Rental(Book Book, Reader Reader) {
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = System.DateTime.Now;
            this.ExpireDate = RentalDate.AddDays(DAYS_OF_RENTAL);
            isReturned = false;
        }

        public Rental(int Id, Book Book, Reader Reader) {
            this.Id = Id;
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = System.DateTime.Now;
            this.ExpireDate = RentalDate.AddDays(DAYS_OF_RENTAL);
            isReturned = false;
        }
        public Rental(Book Book, Reader Reader, DateTime RentalDate) {
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = RentalDate;
            this.ExpireDate = RentalDate.AddDays(DAYS_OF_RENTAL);
            isReturned = false;
        }

        public Rental(Book Book, Reader Reader, DateTime RentalDate, DateTime ExpireDate) {
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = RentalDate;
            this.ExpireDate = ExpireDate;
            isReturned = false;
        }

        public Rental(int Id, Book Book, Reader Reader, DateTime RentalDate, DateTime ExpireDate) {
            this.Id = Id;
            this.Book = Book;
            this.Reader = Reader;
            this.RentalDate = RentalDate;
            this.ExpireDate = ExpireDate;
            isReturned = false;
        }

    }
}
