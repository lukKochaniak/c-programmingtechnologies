﻿using System;

namespace ServicesLayer {
    [Serializable]
    public class Writer {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }

        public Writer(string Name, string Surname) {
            this.Name = Name;
            this.Surname = Surname;
        }

        public Writer(int Id, string Name, string Surname) {
            this.Id = Id;
            this.Name = Name;
            this.Surname = Surname;
        }

        public override string ToString() {
            return Name + " " + Surname;
        }
    }
}
