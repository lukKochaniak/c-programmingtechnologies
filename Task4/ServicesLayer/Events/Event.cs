﻿using System;

namespace ServicesLayer.Events {
    public abstract class Event {
        public delegate void AfterEventExecution(Event ievenet);
        public event AfterEventExecution afterEventExecution;
        public string eventDescription;

        public void eventExecution(Event ievenet) {
            if (afterEventExecution != null) {
                afterEventExecution(ievenet);
                Console.WriteLine(ievenet.eventDescription);
            }
        }

        public string EventDescription() {
            return eventDescription;
        }
    }
}
