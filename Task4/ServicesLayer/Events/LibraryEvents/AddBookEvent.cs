﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events {
    class AddBookEvent : Event {

        public bool Execute(DatabaseDataContext databaseDataContext, Book Book) {
            List<Book> Books = Mapper.mapBooksFromDb(databaseDataContext.GetBooks());

            if (Books.Any(x => x.Title == Book.Title)) {
                Book book = Books.Find(x => x.Title == Book.Title);
                if (book.Title != Book.Title || book.Pages != Book.Pages || book.Year != Book.Year)
                    return false;
                book.Quantity++;
                databaseDataContext.UpdateABook(Mapper.mapBookToDb(book));
                eventDescription = "Increased qunatity of a book with id: " + Book.Id + " and title: " + Book.Title;
                eventExecution(this);
                return true;
            } else {
                Book.Quantity = 1;
                databaseDataContext.AddABook(Mapper.mapBookToDb(Book));
                eventDescription = "Added book with id: " + Book.Id + " and title: " + Book.Title;
                eventExecution(this);
                return true;
            }
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
