﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events {
    class AddReaderEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Reader Reader) {
            List<Reader> Readers = Mapper.mapReadersFromDb(databaseDataContext.GetReaders());

            if (Readers.Contains(Reader)) {
                eventDescription = "Adding reader was unsuccesful";
                return false;
            } else {
                databaseDataContext.AddAReader(Mapper.mapReaderToDb(Reader));
                eventDescription = "Added Reader " + Reader.Name + " " + Reader.Surname;
                eventExecution(this);
                return true;
            }
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
