﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events {
    class AddRentalEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Rental Rental) {
            List<Rental> Rentals = Mapper.mapRentalsFromDb(databaseDataContext.GetRentals());

            if (Rentals.Contains(Rental)) {
                eventDescription = "Adding rental was unsuccesful";
                return false;
            } else {
                databaseDataContext.AddARental(Mapper.mapRentalToDb(Rental));
                eventDescription = "Added Rental on name: " + Rental.Reader.Name + " " + Rental.Reader.Surname + ", date: " + Rental.RentalDate;
                eventExecution(this);
                return true;
            }
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
