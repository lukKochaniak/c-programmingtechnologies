﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events {
    class AddWriterEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Writer writer) {
            List<Writer> Writers = Mapper.mapWritersFromDb(databaseDataContext.GetWriters());

            if (Writers.Contains(writer)) {
                eventDescription = "Adding writer was unsuccesful";
                return false;
            } else {
                databaseDataContext.AddAWriter(Mapper.mapWriterToDb(writer));
                eventDescription = "Added Writer " + writer.Name + " " + writer.Surname;
                eventExecution(this);
                return true;
            }
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
