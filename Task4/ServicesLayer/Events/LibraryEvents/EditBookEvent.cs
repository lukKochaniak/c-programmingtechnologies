﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events.LibraryEvents {
    class EditBookEvent : Event {

        public bool Execute(DatabaseDataContext databaseDataContext, Book editBook) {
            List<Book> Books = Mapper.mapBooksFromDb(databaseDataContext.GetBooks());

            foreach (Book book in Books) {
                if (book.Id == editBook.Id) {
                    eventDescription = "Edited book with id: " + editBook.Id;
                    eventExecution(this);
                    databaseDataContext.UpdateABook(Mapper.mapBookToDb(editBook));
                    return true;
                }
            }

            eventDescription = "Editing book was unsuccesful";
            return false;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
