﻿using DataLayer;
using System.Collections.Generic;

namespace ServicesLayer.Events.LibraryEvents {
    class EditReaderEvent : Event {

        public bool Execute(DatabaseDataContext databaseDataContext, Reader editReader) {
            List<Reader> Readers = Mapper.mapReadersFromDb(databaseDataContext.GetReaders());

            foreach(Reader reader in Readers) {
                if (reader.Id == editReader.Id) {
                    eventDescription = "Edited reader with id: " + editReader.Id;
                    eventExecution(this);
                    databaseDataContext.UpdateAReader(Mapper.mapReaderToDb(editReader));
                    return true;
                }
            }

            eventDescription = "Editing reader was unsuccesful";
            return false;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
