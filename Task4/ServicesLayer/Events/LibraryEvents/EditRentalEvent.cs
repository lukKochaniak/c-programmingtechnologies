﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events.LibraryEvents {
    class EditRentalEvent : Event {

        public bool Execute(DatabaseDataContext databaseDataContext, Rental editRental) {
            List<Rental> Rentals = Mapper.mapRentalsFromDb(databaseDataContext.GetRentals());

            foreach (Rental rental in Rentals) {
                if (rental.Id == editRental.Id) {
                    eventDescription = "Edited rental with id: " + editRental.Id;
                    eventExecution(this);
                    databaseDataContext.UpdateARental(Mapper.mapRentalToDb(editRental));
                    return true;
                }
            }

            eventDescription = "Editing rental was unsuccesful";
            return false;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
