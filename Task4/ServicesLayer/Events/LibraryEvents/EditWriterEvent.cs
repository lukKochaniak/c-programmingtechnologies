﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events.LibraryEvents {
    class EditWriterEvent : Event {

        public bool Execute(DatabaseDataContext databaseDataContext, Writer editWriter) {
            List<Writer> Writers = Mapper.mapWritersFromDb(databaseDataContext.GetWriters());

            foreach(Writer writer in Writers) {
                if(writer.Id == editWriter.Id) {
                    eventDescription = "Edited writer with id: " + editWriter.Id;
                    eventExecution(this);
                    databaseDataContext.UpdateAWriter(Mapper.mapWriterToDb(editWriter));
                    return true;
                }
            }

            eventDescription = "Editing writer was unsuccesful";
            return false;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
