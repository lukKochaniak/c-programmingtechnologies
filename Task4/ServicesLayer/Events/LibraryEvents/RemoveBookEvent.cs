﻿using DataLayer;
using System.Collections.Generic;

namespace ServicesLayer.Events {
    class RemoveBookEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Book Book) {
            List<Book> Books = Mapper.mapBooksFromDb(databaseDataContext.GetBooks());
            eventDescription = "Removed book: " + Book.Title;
            eventExecution(this);
            databaseDataContext.DeleteABook(Book.Id);
            return true;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
