﻿using DataLayer;
using System.Collections.Generic;

namespace ServicesLayer.Events {
    class RemoveReaderEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Reader Reader) {
            List<Reader> Readers = Mapper.mapReadersFromDb(databaseDataContext.GetReaders());
            eventDescription = "Removed reader: " + Reader.Name + " " + Reader.Surname;
            eventExecution(this);
            databaseDataContext.DeleteAReader(Reader.Id);
            return true;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
