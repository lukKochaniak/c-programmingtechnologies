﻿using DataLayer;
using System.Collections.Generic;

namespace ServicesLayer.Events {
    class RemoveRentalEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Rental Rental) {
            List<Rental> Rentals = Mapper.mapRentalsFromDb(databaseDataContext.GetRentals());
            eventDescription = "Removed Rental on name: " + Rental.Reader.Name + " " + Rental.Reader.Surname + ", date: " + Rental.RentalDate + ", expiration date: " + Rental.ExpireDate;
            eventExecution(this);
            databaseDataContext.DeleteARental(Rental.Id);
            return true;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
