﻿using DataLayer;
using System.Collections.Generic;

namespace ServicesLayer.Events {
    class RemoveWriterEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Writer Writer) {
            List<Writer> Writers = Mapper.mapWritersFromDb(databaseDataContext.GetWriters());
            eventDescription = "Removed writer: " + Writer.Name + " " + Writer.Surname;
            eventExecution(this);
            databaseDataContext.DeleteAWriter(Writer.Id);
            return true;
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
