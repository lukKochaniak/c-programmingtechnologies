﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.Events {
    class RenewRentalEvent : Event {
        public bool Execute(DatabaseDataContext databaseDataContext, Rental Rental, DateTime RenewTo) {
            List<Rental> Rentals = Mapper.mapRentalsFromDb(databaseDataContext.GetRentals());
            //int index = Rentals.IndexOf(Rental);
            if (Rentals.Any(x => x.Id == Rental.Id)) {
                Rental.ExpireDate = RenewTo;
                eventDescription = "Renewd Rental on name: " + Rental.Reader.Name + " " + Rental.Reader.Surname + ", date: " + Rental.RentalDate + ", new expiration date: " + Rental.ExpireDate;
                eventExecution(this);
                databaseDataContext.UpdateARental(Mapper.mapRentalToDb(Rental));
                return true;
            } else {
                return false;
            }
        }

        public override string ToString() {
            return eventDescription;
        }
    }
}
