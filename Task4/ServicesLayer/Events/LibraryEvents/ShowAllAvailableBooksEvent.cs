﻿using DataLayer;
using System.Collections.Generic;
using System.Linq;

namespace ServicesLayer.Events.LibraryEvents {
    class ShowAllAvailableBooksEvent : Event {
        public List<Book> Execute(DatabaseDataContext databaseDataContext) {
            List<Rental> Rentals = Mapper.mapRentalsFromDb(databaseDataContext.GetRentals());
            List<Book> Books = Mapper.mapBooksFromDb(databaseDataContext.GetBooks());
            List<Book> availableBooks = new List<Book>();
            foreach (Book book in Books) {
                if (isBookAvailableToRent(Books, Rentals, book)) {
                    availableBooks.Add(book);
                }
            }
            eventDescription = "Showed all available books";
            eventExecution(this);
            return availableBooks;
        }

        private bool isBookAvailableToRent(IList<Book> Books, IList<Rental> Rentals, Book book) {
            if (Books.Any(x => x.Id == book.Id)) {
                bool rented = false;
                foreach (Rental rental in Rentals) {
                    if (book.Id == rental.Book.Id && !rental.isReturned)
                        rented = true;
                }
                return !rented;
            }
            return true;
        }
    }
}
