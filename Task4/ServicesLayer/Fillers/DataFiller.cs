﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DataLayer;

namespace ServicesLayer {
    public class DataFiller : IFiller {
        public void Fill(DatabaseDataContext databaseDataContext) {
            databaseDataContext.CleanEverythingInDB();
            fillWriters(databaseDataContext);
            fillBooks(databaseDataContext);
            fillReaders(databaseDataContext);
            fillRentals(databaseDataContext);
        }

        private void fillBooks(DatabaseDataContext databaseDataContext) {
            databaseDataContext.AddABook(Mapper.mapBookToDb(new Book(1, "testTitleOne", Mapper.mapWriterFromDb(databaseDataContext.GetWriters()[0]), 100, 1990)));
            databaseDataContext.AddABook(Mapper.mapBookToDb(new Book(2, "testTitleTwo", Mapper.mapWriterFromDb(databaseDataContext.GetWriters()[0]), 100, 1990)));
            databaseDataContext.AddABook(Mapper.mapBookToDb(new Book(3, "testTitleThree", Mapper.mapWriterFromDb(databaseDataContext.GetWriters()[0]), 100, 1990)));
        }

        private void fillWriters(DatabaseDataContext databaseDataContext) {
            //databaseDataContext.CleanWriters();
            databaseDataContext.AddAWriter(Mapper.mapWriterToDb(new Writer("nameOne", "surnameOne")));
            databaseDataContext.AddAWriter(Mapper.mapWriterToDb(new Writer("nameTwo", "surnameTwo")));
            databaseDataContext.AddAWriter(Mapper.mapWriterToDb(new Writer("nameThree", "surnameThree")));
        }

        private void fillReaders(DatabaseDataContext databaseDataContext) {
            databaseDataContext.AddAReader(Mapper.mapReaderToDb(new Reader("nameOne", "surnameOne", "City", "Street", "StreetNumber", "FlatNumber")));
            databaseDataContext.AddAReader(Mapper.mapReaderToDb(new Reader("nameTwo", "surnameTwo", "City", "Street", "StreetNumber", "FlatNumber")));
            databaseDataContext.AddAReader(Mapper.mapReaderToDb(new Reader("nameThree", "surnameThree", "City", "Street", "StreetNumber", "FlatNumber")));
        }

        private void fillRentals(DatabaseDataContext databaseDataContext) {
            databaseDataContext.AddARental(Mapper.mapRentalToDb(new Rental(Mapper.mapBookFromDb(databaseDataContext.GetBooks()[0]), Mapper.mapReaderFromDb(databaseDataContext.GetReaders()[0]), new DateTime(1990, 10, 10))));
            databaseDataContext.AddARental(Mapper.mapRentalToDb(new Rental(Mapper.mapBookFromDb(databaseDataContext.GetBooks()[0]), Mapper.mapReaderFromDb(databaseDataContext.GetReaders()[0]), new DateTime(1990, 10, 10))));
            databaseDataContext.AddARental(Mapper.mapRentalToDb(new Rental(Mapper.mapBookFromDb(databaseDataContext.GetBooks()[0]), Mapper.mapReaderFromDb(databaseDataContext.GetReaders()[0]), new DateTime(1990, 10, 10))));
        }
    }
}
