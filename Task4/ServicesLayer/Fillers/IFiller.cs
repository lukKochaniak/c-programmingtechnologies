﻿using DataLayer;

namespace ServicesLayer {
    public interface IFiller {
        void Fill(DatabaseDataContext databaseDataContext);
    }
}
