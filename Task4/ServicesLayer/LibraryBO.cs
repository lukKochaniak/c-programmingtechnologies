﻿using DataLayer;
using ServicesLayer.Events;
using ServicesLayer.Events.LibraryEvents;
using System;
using System.Collections.Generic;
using System.IO;

namespace ServicesLayer {
    public class LibraryBO {
        private DatabaseDataContext databaseDataContext;
        private List<Event> Events;

        public LibraryBO() {
            databaseDataContext = new DatabaseDataContext();
            Events = new List<Event>();
        }

        public LibraryBO(IFiller filler) {
            this.Events = new List<Event>();
            databaseDataContext = new DatabaseDataContext();
            filler.Fill(databaseDataContext);
        }

        public List<Book> returnBooks() {
            List<Book> Books = Mapper.mapBooksFromDb(databaseDataContext.GetBooks());
            return Books;
        }

        public List<Reader> returnReaders() {
            List<Reader> Readers = Mapper.mapReadersFromDb(databaseDataContext.GetReaders());
            return Readers;
        }

        public List<Rental> returnRentals() {
            List<Rental> Rentals = Mapper.mapRentalsFromDb(databaseDataContext.GetRentals()) ;
            return Rentals;
        }

        public List<Writer> returnWriters() {
            List<Writer> Writers = Mapper.mapWritersFromDb(databaseDataContext.GetWriters());
            return Writers;
        }

        public List<string> getEventsDescription() {
            List<string> logs = new List<string>();
            foreach (Event ev in Events) {
                logs.Add(ev.eventDescription);
            }
            return logs;
        }

        public List<Event> getEvents() {
            return Events;
        }

        private void LogAfterEvent(Event addEvent) {
            Events.Add(addEvent);
        }

        public bool addBook(Book Book) {
            AddBookEvent addBookEvent = new AddBookEvent();
            addBookEvent.afterEventExecution += LogAfterEvent;
            return addBookEvent.Execute(databaseDataContext, Book);
        }


        public bool addReader(Reader Reader) {
            AddReaderEvent addReaderEvent = new AddReaderEvent();
            addReaderEvent.afterEventExecution += LogAfterEvent;
            return addReaderEvent.Execute(databaseDataContext, Reader);
        }

        public bool addWriter(Writer Writer) {
            AddWriterEvent addWriterEvent = new AddWriterEvent();
            addWriterEvent.afterEventExecution += LogAfterEvent;
            return addWriterEvent.Execute(databaseDataContext, Writer);
        }

        public bool addRental(Rental Rental) {
            AddRentalEvent addRentalEvent = new AddRentalEvent();
            addRentalEvent.afterEventExecution += LogAfterEvent;
            return addRentalEvent.Execute(databaseDataContext, Rental);
        }

        public bool editBook(Book editBook) {
            EditBookEvent editBookEvent = new EditBookEvent();
            editBookEvent.afterEventExecution += LogAfterEvent;
            return editBookEvent.Execute(databaseDataContext, editBook);
        }

        public bool editRental(Rental editRental) {
            EditRentalEvent editRentalEvent = new EditRentalEvent();
            editRentalEvent.afterEventExecution += LogAfterEvent;
            return editRentalEvent.Execute(databaseDataContext, editRental);
        }

        public bool editReader(Reader editReader) {
            EditReaderEvent editReaderEvent = new EditReaderEvent();
            editReaderEvent.afterEventExecution += LogAfterEvent;
            return editReaderEvent.Execute(databaseDataContext, editReader);
        }

        public bool editWriter(Writer editWriter) {
            EditWriterEvent editWriterEvent = new EditWriterEvent();
            editWriterEvent.afterEventExecution += LogAfterEvent;
            return editWriterEvent.Execute(databaseDataContext, editWriter);
        }

        public bool removeBook(Book Book) {
            RemoveBookEvent removeBookEvent = new RemoveBookEvent();
            removeBookEvent.afterEventExecution += LogAfterEvent;
            return removeBookEvent.Execute(databaseDataContext, Book);
        }

        public bool removeReader(Reader Reader) {
            RemoveReaderEvent removeReaderEvent = new RemoveReaderEvent();
            removeReaderEvent.afterEventExecution += LogAfterEvent;
            return removeReaderEvent.Execute(databaseDataContext, Reader);
        }

        public bool removeRental(Rental Rental) {
            RemoveRentalEvent removeRentalEvent = new RemoveRentalEvent();
            removeRentalEvent.afterEventExecution += LogAfterEvent;
            return removeRentalEvent.Execute(databaseDataContext, Rental);
        }

        public bool removeWriter(Writer writer) {
            RemoveWriterEvent removeWriterEvent = new RemoveWriterEvent();
            removeWriterEvent.afterEventExecution += LogAfterEvent;
            return removeWriterEvent.Execute(databaseDataContext, writer);
        }

        public bool renewRental(Rental Rental, DateTime RenewTo) {
            RenewRentalEvent renewRentalEvent = new RenewRentalEvent();
            renewRentalEvent.afterEventExecution += LogAfterEvent;
            return renewRentalEvent.Execute(databaseDataContext, Rental, RenewTo);
        }

        public void cleanData() {
            databaseDataContext.CleanRentals();
            databaseDataContext.CleanBooks();
            databaseDataContext.CleanReaders();
            databaseDataContext.CleanWriters();
        }
        public void cleanBooks() {
            databaseDataContext.CleanBooks();
        }

        public void cleanReaders() {
            databaseDataContext.CleanReaders();
        }

        public void cleanWriters() {
            databaseDataContext.CleanWriters();
        }

        public void cleanRentals() {
            databaseDataContext.CleanRentals();
        }

        public List<Book> showAllAvailableBooks() {
            ShowAllAvailableBooksEvent showAllAvailableBooksEvent = new ShowAllAvailableBooksEvent();
            showAllAvailableBooksEvent.afterEventExecution += LogAfterEvent;
            return showAllAvailableBooksEvent.Execute(databaseDataContext);
        }

        public void ReadBooksFromFile(string path) {
            using (StreamReader myFile = new StreamReader(path)) {
                while (!myFile.EndOfStream) {
                    string line = myFile.ReadLine();
                    string[] expressions = line.Split(',');
                    Book book = new Book(Int32.Parse(expressions[0]), expressions[1], new Writer(expressions[2], expressions[3]), Int32.Parse(expressions[4]), Int32.Parse(expressions[5]));
                    addBook(book);
                }
            }
        }

        public void ReadReadersFromFile(string path) {
            using (StreamReader myFile = new StreamReader(path)) {
                while (!myFile.EndOfStream) {
                    string line = myFile.ReadLine();
                    string[] expressions = line.Split(',');
                    Reader reader = new Reader(expressions[0], expressions[1], expressions[2], expressions[3], expressions[4], expressions[5]);
                    addReader(reader);
                }
            }
        }

        public void ReadRentalsFromFile(string path) {
            using (StreamReader myFile = new StreamReader(path)) {
                while (!myFile.EndOfStream) {
                    string line = myFile.ReadLine();
                    string[] expressions = line.Split(',');
                    Rental rental = new Rental(new Book(Int32.Parse(expressions[0]), expressions[1], new Writer(expressions[2], expressions[3]), Int32.Parse(expressions[4]), Int32.Parse(expressions[5])),
                         new Reader(expressions[6], expressions[7], expressions[8], expressions[9], expressions[10], expressions[11]), new DateTime(1990, 10, 10));
                    addRental(rental);
                }
            }
        }
    }
}
