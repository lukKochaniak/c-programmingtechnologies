﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer {
    public class Mapper {
        public static Book mapBookFromDb(DataLayer.Book book) {
            return new Book(book.Id, book.Title, new Writer(book.Writer.Id, book.Writer.Name, book.Writer.Surname), book.Pages, book.Year, book.Quantity);
        }

        public static List<Book> mapBooksFromDb(List<DataLayer.Book> DbBooks) {
            List<Book> Books = new List<Book>();
            foreach (DataLayer.Book book in DbBooks) {
                Books.Add(Mapper.mapBookFromDb(book));
            }
            return Books;
        }

        public static DataLayer.Book mapBookToDb(Book book) {
            DataLayer.Book newBook = new DataLayer.Book();
            newBook.Id = book.Id;
            newBook.Pages = book.Pages;
            newBook.Quantity = book.Quantity;
            newBook.Title = book.Title;
            newBook.Year = book.Year;
            newBook.AuthorKey = book.Author.Id;
            //newBook.Writer = mapWriterToDb(book.Author);
            return newBook;
        }

        //to correct
        public static Reader mapReaderFromDb(DataLayer.Reader reader) {
            return new Reader(reader.Id, reader.Name, reader.Surname, reader.City, reader.Street, reader.StreetNumber, reader.StreetNumber);
        }

        public static List<Reader> mapReadersFromDb(List<DataLayer.Reader> DbReaders) {
            List<Reader> Readers = new List<Reader>();
            foreach (DataLayer.Reader reader in DbReaders) {
                Readers.Add(Mapper.mapReaderFromDb(reader));
            }
            return Readers;
        }

        public static DataLayer.Reader mapReaderToDb(Reader reader) {
            DataLayer.Reader newReader = new DataLayer.Reader();
            newReader.Id = reader.Id;
            newReader.Name = reader.Name;
            newReader.City = reader.City;
            newReader.Street = reader.Street;
            newReader.StreetNumber = reader.StreetNumber;
            newReader.Surname = reader.Surname;
            return newReader;
        }

        public static List<Writer> mapWritersFromDb(List<DataLayer.Writer> DbWriters) {
            List<Writer> Writers = new List<Writer>();
            foreach (DataLayer.Writer writer in DbWriters) {
                Writers.Add(Mapper.mapWriterFromDb(writer));
            }
            return Writers;
        }

        public static Writer mapWriterFromDb(DataLayer.Writer writer) {
            return new Writer(writer.Id, writer.Name, writer.Surname);
        }

        public static DataLayer.Writer mapWriterToDb(Writer writer) {
            DataLayer.Writer newWriter = new DataLayer.Writer();
            newWriter.Id = writer.Id;
            newWriter.Name = writer.Name;
            newWriter.Surname = writer.Surname;
            return newWriter;
        }

        public static Rental mapRentalFromDb(DataLayer.Rental rental) {
            return new Rental(rental.Id, new Book(rental.Book.Id, rental.Book.Title, new Writer(rental.Book.Writer.Id, rental.Book.Writer.Name, rental.Book.Writer.Surname), rental.Book.Pages, rental.Book.Year), new Reader(rental.Reader.Id, rental.Reader.Name, rental.Reader.Surname, rental.Reader.City, rental.Reader.Street, rental.Reader.StreetNumber, rental.Reader.StreetNumber), rental.RentalDate, rental.ExpireDate);
        }

        public static List<Rental> mapRentalsFromDb(List<DataLayer.Rental> DbRentals) {
            List<Rental> Rentals = new List<Rental>();
            foreach (DataLayer.Rental rental in DbRentals) {
                Rentals.Add(Mapper.mapRentalFromDb(rental));
            }
            return Rentals;
        }

        public static DataLayer.Rental mapRentalToDb(Rental rental) {
            DataLayer.Rental newRental = new DataLayer.Rental();
            newRental.Id = rental.Id;
            newRental.RentalDate = rental.RentalDate;
            newRental.ExpireDate = rental.ExpireDate;
            newRental.BookKey = rental.Book.Id;
            newRental.ReaderKey = rental.Reader.Id;
            //newRental.Book = Mapper.mapBookToDb(rental.Book);
            return newRental;
        }
    }
}
