﻿using ServicesLayer;
using System;
using System.Collections.Generic;

namespace ServicesLayer {
    public class UserController {
        private LibraryBO libraryBO;

        public UserController() {
            //libraryBO = new LibraryBO();
            libraryBO = new LibraryBO();
        }

        public UserController(LibraryBO libraryBO) {
            this.libraryBO = libraryBO;
        }

        public List<Book> getBooks() {
            return libraryBO.returnBooks();
        }

        public List<Rental> getRentals() {
            return libraryBO.returnRentals();
        }

        public List<Reader> getReaders() {
            return libraryBO.returnReaders();
        }

        public List<Writer> getWriters() {
            return libraryBO.returnWriters();
        }

        public bool addBook(Book book) {
            return libraryBO.addBook(book);
        }

        public bool addReader(Reader reader) {
            return libraryBO.addReader(reader);
        }

        public bool addRental(Rental rental) {
            return libraryBO.addRental(rental);
        }

        public bool editBook(Book editBook) {
            return libraryBO.editBook(editBook);
        }

        public bool editRental(Rental editRental) {
            return libraryBO.editRental(editRental);
        }

        public bool editReader(Reader newReader) {
            return libraryBO.editReader(newReader);
        }

        public bool removeBook(Book book) {
            return libraryBO.removeBook(book);
        }

        public bool removeReader(Reader Reader) {
            return libraryBO.removeReader(Reader);
        }

        public bool removeRental(Rental rental) {
            return libraryBO.removeRental(rental);
        }

        public bool renewRental(Rental rental, DateTime renewTo) {
            return libraryBO.renewRental(rental, renewTo);
        }

        public bool removeWriter(Writer writer) {
            return libraryBO.removeWriter(writer);
        }

        public bool addWriter(Writer newWriter) {
            return libraryBO.addWriter(newWriter);
        }

        public bool editWriter(Writer editedWriter) {
            return libraryBO.editWriter(editedWriter);
        }

        public List<Book> showAllAvailableBooks() {
            return libraryBO.showAllAvailableBooks();
        }

    }
}
