﻿using System;
using System.Collections.Generic;
using DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataLayerUnitTests.DataLayer {
    [TestClass]
    public class DBTest {
        private static DatabaseDataContext databaseDataContext;
        private Writer testWriter;
        private Book testBook;
        private Rental testRental;
        private Reader testReader;

        [TestInitialize]
        public void Setup() {
            initializeTestClasses();
            cleanDb();
        }

        
        [TestCleanup()]
        public void ClassCleanup() {
            cleanDb();
        }

        [TestMethod]
        public void AddWriterTest() {
            //given
            int initialValue = databaseDataContext.GetWriters().Count;

            //when
            databaseDataContext.AddAWriter(testWriter);

            //then
            int finalValue = databaseDataContext.GetWriters().Count;
            Assert.AreEqual(1, finalValue - initialValue);
        }

        [TestMethod]
        public void RemoveWriterTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            int initialValue = databaseDataContext.GetWriters().Count;

            //when
            databaseDataContext.DeleteAWriter(testWriter.Id);

            //then
            int finalValue = databaseDataContext.GetWriters().Count;
            Assert.AreEqual(1, initialValue - finalValue);
        }

        [TestMethod]
        public void UpdateWriterTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);

            Writer newWriter = new Writer();
            newWriter.Id = 1;
            newWriter.Name = "newname";
            newWriter.Surname = "newsurname";

            //when
            databaseDataContext.UpdateAWriter(newWriter);

            //then
            Writer result = databaseDataContext.GetWriters()[0];
            Assert.AreEqual("newname", result.Name);
            Assert.AreEqual("newsurname", result.Surname);
        }

        [TestMethod]
        public void AddBookTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            int initialValue = databaseDataContext.GetBooks().Count;

            //when
            databaseDataContext.AddABook(testBook);

            //then
            int finalValue = databaseDataContext.GetBooks().Count;
            Assert.AreEqual(1, finalValue - initialValue);
        }

        [TestMethod]
        public void RemoveBookTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            databaseDataContext.AddABook(testBook);
            int initialValue = databaseDataContext.GetBooks().Count;

            //when
            databaseDataContext.DeleteABook(testBook.Id);

            //then
            int finalValue = databaseDataContext.GetBooks().Count;
            Assert.AreEqual(1, initialValue - finalValue);
        }

        [TestMethod]
        public void UpdateBookTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            databaseDataContext.AddABook(testBook);

            Book newBook = new Book();
            newBook.Id = 1;
            newBook.Title = "newtitle";
            newBook.AuthorKey = testWriter.Id;

            //when
            databaseDataContext.UpdateABook(newBook);

            //then
            Book result = databaseDataContext.GetBooks()[0];
            Assert.AreEqual("newtitle", result.Title);
        }

        [TestMethod]
        public void AddReaderTest() {
            //given
            int initialValue = databaseDataContext.GetReaders().Count;

            //when
            databaseDataContext.AddAReader(testReader);

            //then
            int finalValue = databaseDataContext.GetReaders().Count;
            Assert.AreEqual(1, finalValue - initialValue);
        }

        [TestMethod]
        public void RemoveReaderTest() {
            //given
            databaseDataContext.AddAReader(testReader);
            int initialValue = databaseDataContext.GetReaders().Count;

            //when
            databaseDataContext.DeleteAReader(testReader.Id);

            //then
            int finalValue = databaseDataContext.GetReaders().Count;
            Assert.AreEqual(1, initialValue - finalValue);
        }

        [TestMethod]
        public void UpdateReaderTest() {
            //given
            databaseDataContext.AddAReader(testReader);

            Reader newReader = new Reader();
            newReader.Id = 1;
            newReader.Name = "newname";
            newReader.City = "newcity";
            newReader.Street = "newstreet";
            newReader.StreetNumber = "newstreetnumber";
            newReader.Surname = "newsurname";

            //when
            databaseDataContext.UpdateAReader(newReader);

            //then
            Reader result = databaseDataContext.GetReaders()[0];
            Assert.AreEqual("newname", result.Name);
        }

        [TestMethod]
        public void AddRentalTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            databaseDataContext.AddABook(testBook);
            databaseDataContext.AddAReader(testReader);
            int initialValue = databaseDataContext.GetRentals().Count;

            //when
            databaseDataContext.AddARental(testRental);

            //then
            int finalValue = databaseDataContext.GetRentals().Count;
            Assert.AreEqual(1, finalValue - initialValue);
        }

        [TestMethod]
        public void RemoveRentalTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            databaseDataContext.AddABook(testBook);
            databaseDataContext.AddAReader(testReader);
            databaseDataContext.AddARental(testRental);
            int initialValue = databaseDataContext.GetRentals().Count;

            //when
            databaseDataContext.DeleteARental(testRental.Id);

            //then
            int finalValue = databaseDataContext.GetRentals().Count;
            Assert.AreEqual(1, initialValue - finalValue);
        }

        [TestMethod]
        public void UpdateRentalTest() {
            //given
            databaseDataContext.AddAWriter(testWriter);
            databaseDataContext.AddABook(testBook);
            databaseDataContext.AddAReader(testReader);
            databaseDataContext.AddARental(testRental);

            Rental newRental = new Rental();
            newRental.Id = 1;
            newRental.RentalDate = new DateTime(2010, 10, 10, 10, 10, 10);
            newRental.ExpireDate = new DateTime(2010, 10, 10, 10, 10, 10);
            newRental.ReaderKey = testReader.Id;
            newRental.BookKey = testBook.Id;

            //when
            databaseDataContext.UpdateARental(newRental);

            //then
            Rental result = databaseDataContext.GetRentals()[0];
            Assert.AreEqual(2010, result.ExpireDate.Year);
            Assert.AreEqual(10, result.ExpireDate.Month);
            Assert.AreEqual(10, result.ExpireDate.Day);

        }

        private void initializeTestClasses() {
            initializeDatabase();
            initializeTestWriter();
            initializeTestBook();
            initializeTestReader();
            initializeTestRental();
        }

        private void cleanDb() {
            databaseDataContext.CleanRentals();
            databaseDataContext.CleanBooks();
            databaseDataContext.CleanReaders();
            databaseDataContext.CleanWriters();
        }

        private void initializeDatabase() {
            databaseDataContext = new DatabaseDataContext();
        }

        private void initializeTestReader() {
            testReader = new Reader();
            testReader.Id = 1;
            testReader.Name = "name";
            testReader.City = "city";
            testReader.Street = "street";
            testReader.StreetNumber = "streetNumber";
            testReader.Surname = "surname";
        }

        private void initializeTestRental() {
            testRental = new Rental();
            testRental.Id = 1;
            testRental.RentalDate = new DateTime(2010, 10, 10);
            testRental.ExpireDate = new DateTime(2010, 10, 10);
            testRental.BookKey = 1;
            testRental.ReaderKey = 1;
            //testRental.Book = testBook;
            //testRental.Reader = testReader;
        }

        private void initializeTestBook() {
            testBook = new Book();
            testBook.Id = 1;
            testBook.Pages = 100;
            testBook.Quantity = 100;
            testBook.Title = "testTitle";
            testBook.Year = 1990;
            testBook.AuthorKey = 1;
            //testBook.AuthorKey = databaseDataContext.GetWriters()[0].Id;
            //testBook.Writer = testWriter;
        }

        private void initializeTestWriter() {
            testWriter = new Writer();
            testWriter.Id = 1;
            testWriter.Name = "name";
            testWriter.Surname = "surname";
        }
    }
}
