﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;

namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class BookViewModelTest {
        [TestMethod]
        public void ConstructorTest() {
            BookViewModel vm = new BookViewModel();
            Assert.IsNotNull(vm.SelectedBook);
            Assert.IsNotNull(vm.ClearBook);
            Assert.IsNotNull(vm.cAddBook);
            Assert.IsNotNull(vm.cEditBook);
            Assert.IsNotNull(vm.cDeleteBook);
        }
    }
}
