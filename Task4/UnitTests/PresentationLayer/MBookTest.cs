﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;

namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class MBookTest {
        [TestMethod]
        public void TestMethod1() {
            MWriter writer = new MWriter(1, "Name", "Surname");
            MBook book = new MBook(1, "Test", writer, "120", "1990", 1);

            Assert.AreEqual(1, book.Id);
            Assert.AreEqual(book.Author, writer);
            Assert.AreEqual(book.Title, "Test");
            Assert.AreEqual(Int32.Parse(book.Pages), 120);
            Assert.AreEqual(Int32.Parse(book.Year), 1990);
            Assert.AreEqual(book.Quantity, 1);
        }
    }
}
