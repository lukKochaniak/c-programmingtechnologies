﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;


namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class MReaderTest {
        [TestMethod]
        public void TestMethod1() {
            MReader reader = new MReader(1, "Name", "Surname", "City", "Street", "90", "210");
            Assert.AreEqual(1, reader.Id);
            Assert.AreEqual("Name", reader.Name);
            Assert.AreEqual("Surname", reader.Surname);
            Assert.AreEqual("City", reader.City);
            Assert.AreEqual("Street", reader.Street);
            Assert.AreEqual(90, Int32.Parse(reader.StreetNumber));
            Assert.AreEqual(210, Int32.Parse(reader.FlatNumber));
        }
    }
}
