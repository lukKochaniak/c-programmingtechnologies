﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;


namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class MRentalTest {
        [TestMethod]
        public void TestMethod1() {
            MWriter writer = new MWriter(1, "Name", "Surname");
            MBook book = new MBook(1, "Test", writer, "120", "1990", 1);
            MReader reader = new MReader(1, "Name", "Surname", "City", "Street", "90", "210");
            DateTime rentalDate = new DateTime(2010, 10, 10);
            DateTime expireDate = new DateTime(2010, 10, 20);
            MRental rental = new MRental(1, book, reader, rentalDate, expireDate);

            Assert.AreEqual(1, rental.Id);
            Assert.AreEqual(book, rental.Book);
            Assert.AreEqual(reader, rental.Reader);
            Assert.AreEqual(rentalDate, rental.RentalDate);
            Assert.AreEqual(expireDate, rental.ExpireDate);
        }
    }
}
