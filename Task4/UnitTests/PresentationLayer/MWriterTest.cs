﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;


namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class MWriterTest {
        [TestMethod]
        public void TestMethod1() {
            MWriter writer = new MWriter(1, "Name", "Surname");

            Assert.AreEqual(1, writer.Id);
            Assert.AreEqual("Name", writer.Name);
            Assert.AreEqual("Surname", writer.Surname);
        }
    }
}
