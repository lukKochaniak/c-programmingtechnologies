﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;

namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class MainViewModelTest {
        [TestMethod]
        public void ConstructorTest() {
            MainViewModel vm = new MainViewModel();
            Assert.IsNotNull(vm.cShowBooks);
            Assert.IsNotNull(vm.cShowReaders);
            Assert.IsNotNull(vm.cShowRentals);
            Assert.IsNotNull(vm.cShowWriters);
        }
    }
}
