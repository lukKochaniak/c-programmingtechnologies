﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;

namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class ReaderViewModelTest {
        [TestMethod]
        public void ConstructorTest() {
            ReaderViewModel vm = new ReaderViewModel();
            Assert.IsNotNull(vm.SelectedReader);
            Assert.IsNotNull(vm.ClearReader);
            Assert.IsNotNull(vm.cAddReader);
            Assert.IsNotNull(vm.cEditReader);
            Assert.IsNotNull(vm.cDeleteReader);
        }
    }
}
