﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;

namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class RentalViewModelTest {
        [TestMethod]
        public void ConstructorTest() {
            RentalViewModel vm = new RentalViewModel();
            Assert.IsNotNull(vm.SelectedRental);
            Assert.IsNotNull(vm.ClearRental);
            Assert.IsNotNull(vm.cAddRental);
            Assert.IsNotNull(vm.cEditRental);
            Assert.IsNotNull(vm.cDeleteRental);
        }
    }
}
