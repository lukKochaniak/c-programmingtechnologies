﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PresentationLayer;

namespace DataLayerUnitTests.PresentationLayer {
    [TestClass]
    public class WriterViewModelTest {
        [TestMethod]
        public void ConstructorTest() {
            WriterViewModel vm = new WriterViewModel();
            Assert.IsNotNull(vm.SelectedWriter);
            Assert.IsNotNull(vm.ClearWriter);
            Assert.IsNotNull(vm.cAddWriter);
            Assert.IsNotNull(vm.cEditWriter);
            Assert.IsNotNull(vm.cDeleteWriter);
        }
    }
}
