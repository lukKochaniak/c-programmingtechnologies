﻿using ServicesLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataLayerUnitTests {
    [TestClass]
    public class LibraryBOTest {
        private LibraryBO testLibraryBO;
        private Writer testWriter;
        private Book testBook;
        private Reader testReader;
        private Rental testRental;
        private DataFiller filler;

        [TestInitialize]
        public void Setup() {
            testLibraryBO = new LibraryBO();
            testWriter = new Writer(1, "Name", "Surname");
            testReader = new Reader(1, "Name", "Surname", "City", "Street", "StreetNumber", "FlatNumber");
            testBook = new Book(1, "Title", testWriter, 10, 1990);
            testRental = new Rental(testBook, testReader, new DateTime(2014, 01, 01), new DateTime(2014, 01, 06));
            filler = new DataFiller();
            testLibraryBO.cleanData();
        }


        [TestMethod]
        public void LibraryBOAddBookTest() {
            //given
            testLibraryBO.addWriter(testWriter);
            Assert.AreEqual(testLibraryBO.returnBooks().Count, 0);

            //when
            Assert.IsTrue(testLibraryBO.addBook(testBook));

            //then
            Assert.AreEqual(testLibraryBO.returnBooks().Count, 1);
        }

        [TestMethod]
        public void LibraryBOAddReaderTest() {
            //given
            testLibraryBO.cleanReaders();
            Assert.AreEqual(testLibraryBO.returnReaders().Count, 0);

            //when
            Assert.IsTrue(testLibraryBO.addReader(testReader));

            //then
            Assert.AreEqual(testLibraryBO.returnReaders().Count, 1);
        }

        [TestMethod]
        public void LibraryBOAddRentalTest() {
            //given
            testLibraryBO.addWriter(testWriter);
            testLibraryBO.addBook(testBook);
            testLibraryBO.addReader(testReader);

            Assert.AreEqual(testLibraryBO.returnRentals().Count, 0);
            Console.WriteLine(testLibraryBO.returnRentals().Count);

            //when
            Assert.IsTrue(testLibraryBO.addRental(testRental));

            //then
            Assert.AreEqual(testLibraryBO.returnRentals().Count, 1);
        }

        [TestMethod]
        public void LibraryBORemoveBookTest() {
            //given
            testLibraryBO.addWriter(testWriter);
            testLibraryBO.addBook(testBook);
            testLibraryBO.addBook(testBook);

            List<Book> Books = testLibraryBO.returnBooks();
            int currentNumberOfBooks = testLibraryBO.returnBooks().Count;

            //when
            Assert.IsTrue(testLibraryBO.removeBook(Books[currentNumberOfBooks-1]));

            //then
            Assert.AreEqual(currentNumberOfBooks - 1, testLibraryBO.returnBooks().Count);
        }

        [TestMethod]
        public void LibraryBORemoveReaderTest() {
            //given
            var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\testFiles\\readers.txt");
            testLibraryBO.ReadReadersFromFile(path);
            List<Reader> Readers = testLibraryBO.returnReaders();
            int currentNumberOfReaders = testLibraryBO.returnReaders().Count;

            //when
            Assert.IsTrue(testLibraryBO.removeReader(Readers[currentNumberOfReaders - 1]));

            //then
            Assert.AreEqual(currentNumberOfReaders - 1, testLibraryBO.returnReaders().Count);
        }

        [TestMethod]
        public void LibraryBORemoveRentalTest() {
            //given
            testLibraryBO.addWriter(testWriter);
            testLibraryBO.addBook(testBook);
            testLibraryBO.addReader(testReader);
            testLibraryBO.addRental(testRental);

            List<Rental> Rentals = testLibraryBO.returnRentals();
            int currentNumberOfRentals = testLibraryBO.returnRentals().Count;

            //when
            Assert.IsTrue(testLibraryBO.removeRental(Rentals[currentNumberOfRentals - 1]));

            //then
            Assert.AreEqual(currentNumberOfRentals - 1, testLibraryBO.returnRentals().Count);
        }


        [TestMethod]
        public void LibraryBORenewRentalWithDateGivenTest() {
            //given
            testLibraryBO.addWriter(testWriter);
            testLibraryBO.addBook(testBook);
            testLibraryBO.addReader(testReader);
            testLibraryBO.addRental(testRental);

            DateTime testRentalDate = new DateTime(2014, 01, 01);
            DateTime testExpireDate = new DateTime(2014, 01, 06);
            DateTime testExpireDateRenewed = new DateTime(2014, 01, 20);

            Rental newRental = new Rental(1, testBook, testReader, testRentalDate, testExpireDateRenewed);

            //when
            testLibraryBO.editRental(newRental);

            //then
            List<Rental> Rentals = testLibraryBO.returnRentals();
            Assert.AreEqual(testExpireDateRenewed, Rentals[0].ExpireDate);
        }

        [TestMethod]
        public void LibraryBOShowAvailableBooksTest() {
            //given
            //var path = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\testFiles\\books.txt");
            //testLibraryBO.ReadBooksFromFile(path);

            //when
            List<Book> result = testLibraryBO.showAllAvailableBooks();

            //then
            Assert.AreEqual(0, result.Count);
        }
    }
}
