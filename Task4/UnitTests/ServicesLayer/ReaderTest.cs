﻿using ServicesLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataLayerUnitTests {
    [TestClass]
    public class ReaderTest {
        [TestMethod]
        public void ReaderConstructorTest() {
            Reader testReader = new Reader("Name", "Surname", "City", "Street", "StreetNumber", "FlatNumber");

            Assert.AreEqual("Name", testReader.Name);
            Assert.AreEqual("Surname", testReader.Surname);
            Assert.AreEqual("City", testReader.City);
            Assert.AreEqual("Street", testReader.Street);
            Assert.AreEqual("StreetNumber", testReader.StreetNumber);
            Assert.AreEqual("FlatNumber", testReader.FlatNumber);
            }
        }
    }
