﻿using DataLayer;
using ServicesLayer;
using System;
using System.Collections.Generic;

namespace DataLayerUnitTests {
    public class TestDataFiller : IFiller {
        public void Fill(DatabaseDataContext databaseDataContext) {
            databaseDataContext.CleanEverythingInDB();
            fillWriters(databaseDataContext);
            fillBooks(databaseDataContext);
            fillReaders(databaseDataContext);
            fillRentals(databaseDataContext);
        }

        private void fillBooks(DatabaseDataContext databaseDataContext) {
            databaseDataContext.AddABook(Mapper.mapBookToDb(new ServicesLayer.Book(1, "testTitleOne", Mapper.mapWriterFromDb(databaseDataContext.GetWriters()[0]), 100, 1990)));
            databaseDataContext.AddABook(Mapper.mapBookToDb(new ServicesLayer.Book(2, "testTitleTwo", Mapper.mapWriterFromDb(databaseDataContext.GetWriters()[0]), 100, 1990)));
            databaseDataContext.AddABook(Mapper.mapBookToDb(new ServicesLayer.Book(3, "testTitleThree", Mapper.mapWriterFromDb(databaseDataContext.GetWriters()[0]), 100, 1990)));
        }

        private void fillWriters(DatabaseDataContext databaseDataContext) {
            //databaseDataContext.CleanWriters();
            databaseDataContext.AddAWriter(Mapper.mapWriterToDb(new ServicesLayer.Writer("nameOne", "surnameOne")));
            databaseDataContext.AddAWriter(Mapper.mapWriterToDb(new ServicesLayer.Writer("nameTwo", "surnameTwo")));
            databaseDataContext.AddAWriter(Mapper.mapWriterToDb(new ServicesLayer.Writer("nameThree", "surnameThree")));
        }

        private void fillReaders(DatabaseDataContext databaseDataContext) {
            databaseDataContext.AddAReader(Mapper.mapReaderToDb(new ServicesLayer.Reader("nameOne", "surnameOne", "City", "Street", "StreetNumber", "FlatNumber")));
            databaseDataContext.AddAReader(Mapper.mapReaderToDb(new ServicesLayer.Reader("nameTwo", "surnameTwo", "City", "Street", "StreetNumber", "FlatNumber")));
            databaseDataContext.AddAReader(Mapper.mapReaderToDb(new ServicesLayer.Reader("nameThree", "surnameThree", "City", "Street", "StreetNumber", "FlatNumber")));
        }

        private void fillRentals(DatabaseDataContext databaseDataContext) {
            databaseDataContext.AddARental(Mapper.mapRentalToDb(new ServicesLayer.Rental(Mapper.mapBookFromDb(databaseDataContext.GetBooks()[0]), Mapper.mapReaderFromDb(databaseDataContext.GetReaders()[0]), new DateTime(1990, 10, 10))));
            databaseDataContext.AddARental(Mapper.mapRentalToDb(new ServicesLayer.Rental(Mapper.mapBookFromDb(databaseDataContext.GetBooks()[0]), Mapper.mapReaderFromDb(databaseDataContext.GetReaders()[0]), new DateTime(1990, 10, 10))));
            databaseDataContext.AddARental(Mapper.mapRentalToDb(new ServicesLayer.Rental(Mapper.mapBookFromDb(databaseDataContext.GetBooks()[0]), Mapper.mapReaderFromDb(databaseDataContext.GetReaders()[0]), new DateTime(1990, 10, 10))));
        }
    }
}
