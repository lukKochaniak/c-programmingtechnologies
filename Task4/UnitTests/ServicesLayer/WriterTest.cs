﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServicesLayer;

namespace DataLayerUnitTests {
    [TestClass]
    public class WriterTest {
        [TestMethod]
        public void WriterConstructorTest() {
            Writer testWriter = new Writer("Name", "Surname");

            Assert.AreEqual("Name", testWriter.Name);
            Assert.AreEqual("Surname", testWriter.Surname);
            }
        }
    }
